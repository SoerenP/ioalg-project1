# Project1 Of IO Alg at Aarhus University :)

#### Contributors: Søren Elmely Pettersson & Troels Thorsen

####Known Bugs:
Stream D eof is based on 0's being in the last part of the mapped file, and reading a 0 indicates eof, as such you cannot write 0's using stream d and expect eof functionality to work. 


The test for mergesort using Stream C leaks a little (a fraction of the test size), we've not been able to fix this. 

####Missing implementations

Merging and mergesort for stream D are not completed. In their current state they do not work for all combinations of parameters N,M and d. 

####Compatability:
The make file assumes UBUNTU as environment when linking. Linking static libraries in C can depend on the order of
the arguments when linking. On Ubuntu these should be after the object files. In OSX i believe the reverse is true.
The make file works fine on Arch Linux too.

GNUPLOT scripts assume gnuplot v.5.0 as they use extensively the command line passing functionality -c introduced in v.5.0

external Mergesort For stream B assumes that N and M are powers of two (or that M divides N perfectly).

ext. mergesort and merging for streams A and C are not optimized and quite naive (use way too much main memory for practical use for large N, M).

#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <string.h>

#include <IOAlg/dbg.h>
#include <IOAlg/Mergesort.h>
#include <IOAlg/StreamB.h>

#define EXPECTED_ARGS 4
#define PATH_SIZE 100

/* kørsel hvis du står i bin mappen er Experiment_MergeSort_StreamB 1024 64 2 tests/testfiles/ */

static const char *unsorted_file = "mergesortBunsort";
static const char *resulting_file = "mergesortBsort";
static char *filepath_prefix = NULL;

int main(int argc, char **argv)
{

	if(argc < EXPECTED_ARGS)
	{
		printf("Usage: %s N (#elements total), M (#elements pr stream), d (#merging streams each iteration), fileprefix\n",argv[0]);
		exit(1);
	}

	srand(time(NULL));

	int N = atoi(argv[1]);
	int M = atoi(argv[2]);
	int d = atoi(argv[3]);
	char buffer_in[PATH_SIZE] = {0};
	char buffer_out[PATH_SIZE] = {0};
	filepath_prefix = argv[4];

	snprintf(buffer_in,sizeof(char)*PATH_SIZE,"%s%s",filepath_prefix,unsorted_file);
	snprintf(buffer_out,sizeof(char)*PATH_SIZE,"%s%s",filepath_prefix,resulting_file);

	printf("#----MergeSort External Using Stream B, N = %d, M = %d, d = %d----\n",N,M,d);

	/* make a stream with unsorted data */
	//int *random_array = malloc(sizeof(int) * N);
	//check_mem(random_array);
	int index;
	//for(index = 0; index < N; index++) random_array[index] = rand();

	OutputStreamB *unsorted = OutputStreamB_create(buffer_in,sizeof(int),N);

	//OutputStreamB_write(unsorted,random_array,N);
	//free(random_array);
	for(index = 0; index < N; index++){
		int buf = rand();
		OutputStreamB_write(unsorted,&buf,1);
	}

	OutputStreamB_close(unsorted);

	/* start time */
	clock_t begin,end;
	double time_spent;

	begin = clock();

	/* run mergesort */
	int rc = mergesort_ext_B(buffer_in,buffer_out,N,M,d);

	/* stop the time */
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

	fprintf(stderr,"rc %d\n",rc);
	/* cleanup */

	printf("#N \t#M \t#d \t\t#sorting_time\n");
	printf("%d \t%d \t%d \t\t%f\n",N,M,d,time_spent);

	return 0;
}

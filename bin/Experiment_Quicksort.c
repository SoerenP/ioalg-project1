#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <IOAlg/Quicksort.h>
#include <IOAlg/dbg.h>

#define EXPECTED_ARGS 2

int main(int argc, char **argv)
{
	if(argc != EXPECTED_ARGS)
	{
		printf("Usage: %s N (numbers to sort)\n",argv[0]);
		exit(1);
	}

	srand(time(NULL));

	int N = atoi(argv[1]);
	int *test_array = malloc(sizeof(int) * N);
	check_mem(test_array);

	int index;
	for(index = 0; index < N; index++)
	{
		test_array[index] = rand();
	}

	printf("#----Running Experiment Quicksort with N = %d----\n",N);
	printf("#N \t #Sorting Time\n");

	clock_t begin, end;
	double time_spent;

	begin = clock();

	quicksort(test_array,N);

	end = clock();

	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("%d \t%f\n",N,time_spent);

	free(test_array);
	return 0;
error:
	if(test_array) free(test_array);
	return -1;
}

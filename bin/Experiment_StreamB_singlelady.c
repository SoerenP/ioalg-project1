#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <IOAlg/StreamB.h>


#define PATH_SIZE (100)
#define BLOCK_SIZE (sizeof(int))
#define EXPECTED_ARGS 4
#define RW (0)
#define ALL (1)
static int exp_counter = 0;
static char *exp_path  = "../Exp";

void exp_counter_reset()
{
	exp_counter = 0;
}

void next_stream_name(char *buffer, size_t strlen)
{
	//Reset Buffer
	memset(buffer,0,strlen);
	
	//Print over new name
	snprintf(buffer,
		sizeof(char)*PATH_SIZE,
		"%s%d",
		exp_path,
		exp_counter);

	exp_counter++;
}

int main(int argc, char **argv)
{
	if(argc < EXPECTED_ARGS){
		printf("Usage: sudo %s k (# streams), N (#read/writes), filepath prefix, timing (rw = 0 or all = 1) \n",argv[0]);
		exit(0);
	}

	srand(time(NULL));

	char buffer[PATH_SIZE] = {0};
	int k = atoi(argv[1]);
	int N = atoi(argv[2]);
	int timing = atoi(argv[4]);
	exp_path = argv[3];
	int index = 0;
	int inner = 0;

	int *random_array = malloc(sizeof(int) * N);
	int *res_array = malloc(sizeof(int) * N);
	if(!(res_array) || !(random_array)) exit(1);

	printf("#----Running Experiment StreamB with k = %d, N = %d, timing = %d ----\n",k,N,timing);
	
	printf("#N \t#K \t#write \t\t#read\n");
	/* setup random numbers */
	for(index = 0; index < N; index++) random_array[index] = rand();

	/* start timer */
	clock_t begin = 0.0;
	clock_t end = 0.0;
	double time_spent;
	
	if(timing == ALL) begin = clock();

	/* create k streams output */
	OutputStreamB *o_streams[k];
	for(index = 0; index < k; index++)
	{
		next_stream_name(buffer, PATH_SIZE);
		o_streams[index] = 
		OutputStreamB_create(buffer, BLOCK_SIZE, N);
	}


	if(timing == RW) begin = clock();

	/* write random stuff to streams */
	for(inner = 0; inner < k; inner++)
	{
		OutputStreamB_write(o_streams[inner],
			random_array,N);
	}

	/* stop counting */
	if(timing == RW) end = clock();

	/* cleanup k streams output streams */
	for(index = 0; index < k; index++)
	{
		OutputStreamB_close(o_streams[index]);
	}

	if(timing == ALL) end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	
	/* print out data */
	printf("%d",N);
	printf("\t%d",k);
	printf("\t%f",time_spent);

	/***** TIME TO READ ******/

	/*reset stream names */
	exp_counter_reset();

	begin = 0.0;
	end = 0.0;

	if(timing == ALL) begin = clock();

	/* create k streams input */
	InputStreamB *i_streams[k];
	for(index = 0; index < k; index++)
	{
		next_stream_name(buffer, PATH_SIZE);
		i_streams[index] = 
		InputStreamB_open(buffer, BLOCK_SIZE, N);	
	}

	if(timing == RW) begin = clock();

    int very_inner;
	/*read random crap from streams */
	for(inner = 0; inner < k; inner++)
      {
        for (very_inner = 0; very_inner < N; very_inner++){
          InputStreamB_read_next(i_streams[inner], &res_array[very_inner], 1);
        }
	}

	if(timing == RW) end = clock();
	
	/* cleanup k streams input */
	for(index = 0; index < k; index++)
	{
		InputStreamB_close(i_streams[index]);
	}

	if(timing == ALL) end = clock();


	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("\t%f\n",time_spent);
	

	/* use res array so O2 doesnt remove the function calls */
	int sum = 0;
	int i;
	for (i = 0; i < N; i++){
		sum += res_array[i];
	}
	fprintf(stderr,"sum : %d\n",sum);
	
	/* cleanup arrays */
	if(res_array) free(res_array);
	if(random_array) free(random_array);
	
	return 0;
}

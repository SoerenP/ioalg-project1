
#usage: bash mergesort_test_d.sh | tee datafil.dat
#!/bin/bash
n=$((2**28))
m=$((2**21))
for i in 1 2 3 4 5 6 7
do
	bash run_mergesort_experiment.sh $n $m $((2**i)) tests/testfiles/
done

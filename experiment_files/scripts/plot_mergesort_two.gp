#Usage: gnuplot -c plot_readwrite_two.gp fil1 fil2 title1
#!/usr/local/bin/gnuplot

#plot all readtimes
set title ARG3
set xlabel "d"
set ylabel "Sorting time"
plot 	ARG1 using 3:4 with linespoints title "All elements in Heap", \
	ARG2 using 3:4 with linespoints title "d elements in Heap"
pause -1 "Hit any key to continue"


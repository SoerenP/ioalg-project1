#Usage: gnuplot -c plot_mergesort_varying_d_m.gp fil1 fil2 fil3 fil4 fil5 fil6 fil7 fil8 fil9 
#!/usr/local/bin/gnuplot

#plot all readtimes
set title "External Mergesort, N = pow(2,28), varying d,m"
set xlabel "d"
set logscale x 2
set ylabel "Sorting Time Seconds"
plot 	ARG1 using 3:4 with linespoints title "M=pow(2,22)", \
	ARG2 using 3:4 with linespoints title "M=pow(2,23)", \
	ARG3 using 3:4 with linespoints title "M=pow(2,24)", \
	ARG4 using 3:4 with linespoints title "M=pow(2,25)", \
	ARG5 using 3:4 with linespoints title "M=pow(2,26)", \
	ARG6 using 3:4 with linespoints title "M=pow(2,27)"
pause -1 "Hit any key to continue"


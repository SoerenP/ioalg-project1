# Usage: 
#	gnuplot -c plot_readwrite_B filnavn title
#!/usr/local/bin/gnuplot

#plot versus readtime
set title ARG2
set xlabel "log Blocksize"
set logscale x 10
set ylabel "readtime"
plot ARG1 using 5:4 with linespoints title ARG1
pause -1 "Hit any key to continue"

#plot versus writetime
set title ARG2
set xlabel "log Blocksize"
set logscale x 10
set ylabel "writetime"
plot ARG1 using 5:3 with linespoints title ARG1
pause -1 "Hit any key to continue"

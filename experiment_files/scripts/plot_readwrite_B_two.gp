# Usage: 
#	gnuplot -c plot_readwrite_B fil1 fil2 title1 title2
#!/usr/local/bin/gnuplot

#plot versus readtime
set title ARG3
set xlabel "log Blocksize"
set logscale x 2
set logscale y 2
set ylabel "readtime"
plot 	ARG1 using 5:4 with linespoints title ARG1, \
	ARG2 using 5:4 with linespoints title ARG2
pause -1 "Hit any key to continue"

#plot versus writetime
set title ARG4
set xlabel "log Blocksize"
set logscale x 2
set logscale y 2
set ylabel "writetime"
plot 	ARG1 using 5:3 with linespoints title ARG1, \
	ARG2 using 5:3 with linespoints title ARG2
pause -1 "Hit any key to continue"

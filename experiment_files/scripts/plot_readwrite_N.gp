# Usage:
#	gnuplot -c plot_readwrite_N filename title

#first we plot vs readtime
set title ARG2
set xlabel "Streams"
set ylabel "readtime"
plot ARG1 using 2:4 with linespoints title ARG1
pause -1 "Hit any key to continue"
#then we plot vs writetime
set title ARG2
set xlabel "Streams"
set ylabel "writetime"
plot ARG1 using 2:3 with linespoints title ARG1
pause -1 "Hit any key to continue"

#Usage: gnuplot -c plot_readwrite_all.gp fil1 fil2 fil3 fil4 title1 title2
#!/usr/local/bin/gnuplot

#plot all readtimes
set title ARG5
set xlabel "Streams"
set logscale x 2
set logscale y 2
set ylabel "readtime"
plot 	ARG1 using 2:4 with linespoints title "A", \
	ARG2 using 2:4 with linespoints title "B", \
	ARG3 using 2:4 with linespoints title "C", \
	ARG4 using 2:4 with linespoints title "D"
pause -1 "Hit any key to continue"

#plot all writetimes
set title ARG6
set xlabel "Streams"
set logscale x 2
set logscale y 2
set ylabel "writetime"
plot 	ARG1 using 2:3 with linespoints title "A", \
	ARG2 using 2:3 with linespoints title "B", \
	ARG3 using 2:3 with linespoints title "C", \
	ARG4 using 2:3 with linespoints title "D"
pause -1 "Hit any key to continue"

#Usage: gnuplot -c plot_readwrite_two.gp fil1 fil2 title1 title2
#!/usr/local/bin/gnuplot

#plot all readtimes
set title ARG3
set xlabel "Streams"
set logscale x 2
set logscale y 2
set ylabel "readtime"
plot 	ARG1 using 2:4 with linespoints title "StreamB", \
	ARG2 using 2:4 with linespoints title "StreamC"
pause -1 "Hit any key to continue"

#plot all writetimes
set title ARG4
set xlabel "Streams"
set logscale x 2
set logscale y 2
set ylabel "writetime"
plot 	ARG1 using 2:3 with linespoints title "StreamB", \
	ARG2 using 2:3 with linespoints title "StreamC"
pause -1 "Hit any key to continue"

#usage: run_and_plot_Stream_Blocksize.sh stream resultfile.data n k timing
# stream = {c,d}
# n = nr. elements
# b = blocksize
# timing = (rw = 0, all = 1)
#!/bin/bash
n=$3
k=$4
timing=$5
echo "Running run_eperiments_stream_$1.sh"
bash stream$1_test_B.sh $k $n $timing | tee ../experiment_result/$2

#plot it with gnuplot
echo "Plotting with gnuplot"
if [ $1 = c ] || [ $1 = d ]
	then gnuplot -c plot_readwrite_B.gp ../experiment_result/$2 "N="$n
fi

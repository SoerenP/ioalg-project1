#run experiment time, pipe it to file, call gnuplot
#Usage: run_and_plot_stream_rw.sh stream temp.data n b timing
# stream: can be either the single character a,b,c or d, corresponding to a given stream implementation
# temp.data: the file the data is written to, placed in the experiment_result folder. It will be overwritten automatically by tee, so
# be careful if you want to save some data from a single run!
# n: the amount of elements in the single tests to be read / written pr stream
# b: blocksize for implementation c and d, has no effect on a and b.
# timing: 0 (time rw only) 1 (time rw and create/open/close overhead of stream)
#!/bin/bash
n=$3
b=$4
timing=$5
echo "Running run_experiments_stream_$1.sh"
if [ $1 = a ] || [ $1 = b ]
	then ./run_experiments_stream_$1.sh $n $timing | tee ../experiment_result/$2
elif [ $1 = c ] || [ $1 = d ]
	then ./run_experiments_stream_$1.sh $n $b $timing | tee ../experiment_result/$2
fi
#plot it with gnuplot
echo "Plotting with gnuplot"
if [ $1 = a ] || [ $1 = b ] || [ $1 = c ] || [ $1 = d ]
	then gnuplot -c plot_readwrite_N.gp ../experiment_result/$2 "N="$n
fi

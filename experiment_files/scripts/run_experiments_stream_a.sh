#run experiment, k streams, n read/writes
# n = arg1
#!/bin/bash
n=$1
timing=$2
for i in 1 2 3 4 5 6 7 8 9 10
do
	../../bin/Experiment_StreamA $((i*100-1)) $n ../Exp $timing

	#cleanup
	rm -f ../Exp*
done

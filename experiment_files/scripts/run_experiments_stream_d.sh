#run experiments, k stream, n read/writes
# StreamDmaxes out at ? streams (OS Says no more)
#!/bin/bash
n=$1
b=$2
timing=$3
for i in 1 2 3 4 5 6 7 8 9 10
do
	../../bin/Experiment_StreamD $((i*100-1)) $n $b ../Exp $timing

	#cleanup
	rm -f ../Exp*

done

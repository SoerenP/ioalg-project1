#usage bash run_mergesort_experiment.sh n m d test/testfiles/
#!/bin/bash
n=$1
m=$2
d=$3
filepath=$4
cd ../../bin/
Experiment_Mergesort_StreamB $n $m $d $filepath

#cleanup
rm -f tests/testfiles/*
cd ../experiment_files/scripts

#!/bin/bash
echo "Running Experiment for ordinary sorting!"
echo "First quicksort"
./run_quicksort_timing.sh | tee ../experiment_result/$1

echo "Now Heapsort"
./run_heapsort_timing.sh | tee ../experiment_result/$2

#run streamC for static n,k and B = 10^1 to 10^6
#remember to pipe it into something. 
#!/bin/bash
k=$1
n=$2
timing=$3
for i in 1 2 3 4 5 6 7 8 9 10 11 12
do
	../../bin/Experiment_StreamD $k $n $((4096*(2**i))) ../Exp $timing

	#cleanup
	rm -f ../Exp*
done

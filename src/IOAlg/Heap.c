#include <IOAlg/Heap.h>
#include <IOAlg/dbg.h>
#include <stdlib.h>

static int double_the_space(Heap *Q){
  int *temp = realloc(Q->elements, 2*Q->maxsize*sizeof(int));
  check_mem(temp);

  Q->elements = temp;
  Q->maxsize = Q->maxsize*2;

  return HEAP_TRUE;
 error:
  return HEAP_FALSE;
}

static int isFull(Heap *Q){
  if (Q->idx >= Q->maxsize)
    return HEAP_TRUE;
  return HEAP_FALSE;
}

static int isEmpty(Heap *Q){
  if (Q->idx <= 0)
    return HEAP_TRUE;
  return HEAP_FALSE;
}

static void swap(Heap *Q, int i, int j) {
  int temp = Q->elements[i];
  Q->elements[i] = Q->elements[j];
  Q->elements[j] = temp;
}


// Heap implementation
Heap* heap_makeHeap() {
  Heap *Q = malloc(sizeof(*Q));
  check_mem(Q);
  Q->idx = 0;
  Q->maxsize = 128;
  Q->elements = malloc(Q->maxsize * sizeof(int));
  check_mem(Q->elements);

  return Q;
 error:
  return NULL;
}

void heap_destroy(Heap *Q){
  free(Q->elements);
  free(Q);
}

int heap_insert(Heap *Q, int i) {
  if (isFull(Q) == HEAP_TRUE){
    check(double_the_space(Q) == HEAP_TRUE, "Could not double the space of array.");
  }
  Q->elements[Q->idx] = i;
  Q->idx++;

  int inserted = Q->idx - 1;
  int indexOfParent = (inserted + 1) / 2 - 1;
  while (0 < inserted &&
         Q->elements[inserted] < Q->elements[indexOfParent]) {
    swap(Q, inserted, indexOfParent);
    inserted = indexOfParent;
    indexOfParent = (inserted + 1) / 2 - 1;
  }
  return HEAP_TRUE;
  
 error:
  return HEAP_FALSE;
}

int heap_deleteMin(Heap *Q) {
  if (isEmpty(Q) == HEAP_TRUE){
    return HEAP_FALSE;
  }

  int ret = Q->elements[0];
  swap(Q, 0, Q->idx - 1);
  Q->idx--;

  int size = Q->idx;
  int nodeToBeFixed = 0;
  int childL = (nodeToBeFixed + 1) * 2 - 1;
  int childR = (nodeToBeFixed + 1) * 2;

  while (1) {
    // If nodeToBeFixed doesn't have any children.
    if (size <= childL) break;

    // If nodeToBeFixed only has a left child.
    if (size == childR) {
      if (Q->elements[childL] < Q->elements[nodeToBeFixed]){
        swap(Q,nodeToBeFixed, childL);
      }
      break;
    }

    // If nodeToBeFixed has both children.
    // Find the smallest of the two children
    if (Q->elements[childL] < Q->elements[childR] &&
        Q->elements[childL] < Q->elements[nodeToBeFixed]) {
      swap(Q, nodeToBeFixed, childL);
      nodeToBeFixed = childL;
      childL = (nodeToBeFixed + 1) * 2 - 1;
      childR = (nodeToBeFixed + 1) * 2;
    }
    // We know that the left child couldn't have been the smallest node of the three.
    // So, we test whether the right child is smaller than nodeToBeFixed.
    else if (Q->elements[childR] < Q->elements[nodeToBeFixed]) {
      swap(Q, nodeToBeFixed, childR);
      nodeToBeFixed = childR;
      childL = (nodeToBeFixed + 1) * 2 - 1;
      childR = (nodeToBeFixed + 1) * 2;
    } else {
      break;
    }
  }

  return ret;
}


/* overrides the input array to be sorted */
void heapsort_inplace(int *A, int size)
{
	Heap *H = heap_makeHeap();
	check_mem(H);

	int index = 0;
	for(index = 0; index < size; index++)
	{
		heap_insert(H,A[index]);
	}

	for(index = 0; index < size; index++)
	{
		A[index] = heap_deleteMin(H);
	}

	if(H) heap_destroy(H);
error:
	return;
}


int *heapsort(int *A, int size)
{
	Heap *H = heap_makeHeap();
	check_mem(H);

	int *sorted = malloc(sizeof(int) * size);
	check_mem(sorted);

	int index = 0;
	for(index = 0; index < size; index++)
	{
		heap_insert(H,A[index]);
	}

	for(index = 0; index < size; index++)
	{
		sorted[index] = heap_deleteMin(H);
	}

	if(H) heap_destroy(H);
	return sorted;
error:
	if(H) heap_destroy(H);
	return NULL;
}

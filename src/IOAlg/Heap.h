#ifndef __HEAP_H_
#define __HEAP_H_

typedef struct Heap {
  int idx;
  int* elements;
  int maxsize;
} Heap;

#define HEAP_TRUE 0
#define HEAP_FALSE -1

/// PUBLIC
Heap *heap_makeHeap();
int heap_insert(Heap *Q, int i);
int heap_deleteMin(Heap *Q);
void heap_destroy(Heap *Q);
int *heapsort(int *A, int size);
void heapsort_inplace(int *A, int size);

#endif // __HEAP_H_

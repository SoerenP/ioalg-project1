#include <IOAlg/Mergesort.h>
#include <IOAlg/bool.h>
#include <IOAlg/Quicksort.h>
#include <IOAlg/dbg.h>
#include <IOAlg/merging.h>
#include "math.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#define PATH_SIZE (64)
#define MINIMAL_D 2

static char *fileprefix_A = "tests/testfiles/fragment_a";
static char *fileprefix_B = "tests/testfiles/fragment_b";
static char *fileprefix_C = "tests/testfiles/fragment_c";
static char *fileprefix_D = "tests/testfiles/fragment_d";

void print_arr(int *A, int size)
{
  int index;
  for(index = 0; index < size; index++)
    {
      printf("%d ",A[index]);
    }
  printf("\n");
}

void next_stream_name(char *buffer, size_t strlen, int exp_counter, const char *prefix)
{
  //Reset Buffer
  memset(buffer,0,strlen);

  //Print over new name
  snprintf(buffer,
           sizeof(char)*PATH_SIZE,
           "%s%d",
           prefix,
           exp_counter);
}

static void close_streama_array(InputStreamA **arr, int amount){
  int i;
  for (i = 0; i < amount; i++){
    if (arr[i] != NULL)
      InputStreamA_close(arr[i]);
  }
}

int mergesort_ext_A(const char *inputfilename, const char *outputfilename, int N, int M, int d){
  // D cannot be smaller than 2.
  check( d >= MINIMAL_D , "Invalid d!");

  int number_of_streams = (int) ceil(N/M);
  int i, j, h;
  char buffer[PATH_SIZE] = {0};
  int *a = malloc(sizeof(int) * M);
  check_mem(a);

  // Open file
  InputStreamA *inputstream = InputStreamA_open(inputfilename, sizeof(int));
  check(inputstream != NULL, "inputstream was null");

  int buf = 0;
  // Create streams for each fragment of the file and sort them.
  for (i = 0; i < number_of_streams; i++){
    for (j = 0; j < M; j++){
      if (InputStreamA_read_next(inputstream, &buf) == STREAM_ERR){
        break;
      }

      if (InputStreamA_end_of_stream(inputstream) != FALSE){
        break;
      }

      a[j] = buf;
    }
    // sort
    quicksort(a, j);
    // get a new name of a file
    next_stream_name(buffer, PATH_SIZE, i,fileprefix_A);
    OutputStreamA *outputstream = OutputStreamA_create(buffer, sizeof(int));
    // write to disk
    for (h = 0; h < j; h++){
      OutputStreamA_write(outputstream, &a[h]);
    }
    // done, close it for use later.
    OutputStreamA_close(outputstream);
  }
  // close inputstream
  InputStreamA_close(inputstream);

  // allocate double space, this should be enough if d > 1
  // we use O(n log_d n) streams for this
  InputStreamA **inputstreams = calloc(number_of_streams*2,sizeof(InputStreamA*));
  check_mem(inputstreams);
  // make M inputstreams for merging, using the sorted files we spawned earlier
  for (i = 0; i < number_of_streams; i++){
    next_stream_name(buffer, PATH_SIZE, i, fileprefix_A);
    InputStreamA *input = InputStreamA_open(buffer, sizeof(int));
    check(input != NULL, "input was null, iteration was %d", i);
    inputstreams[i] = input;
  }

  int streamsleft = number_of_streams;
  int end_of_array = number_of_streams;
  int index_to_eat = 0;
  int namecounter = 0;
  OutputStreamA *current;
  while(streamsleft > 1){
    /* fprintf(stderr, "eat index : %d, end_of_array : %d, namecounter : %d, streamsleft : %d, \n" */
    /*         ,index_to_eat, end_of_array, namecounter, streamsleft); */
    // get a fresh fragment name
    next_stream_name(buffer, PATH_SIZE, namecounter+number_of_streams,fileprefix_A);
    // eat d or less streams
    if (streamsleft <= d){
      current = StreamA_merge(outputfilename, inputstreams + index_to_eat, streamsleft, sizeof(int));
      // close old, unused streams
      close_streama_array(inputstreams + index_to_eat, streamsleft);

      // close what we got back
      OutputStreamA_close(current);

      fprintf(stderr, "Mergesort A should be done.");

      // We're done, we had less than d streams left, and merged them into one.
      // We don't add a new stream here.
      break;
    }
    // We are guaranteed to have at least 2 streams left after this iteration
    else {
      current = StreamA_merge(buffer, inputstreams + index_to_eat, d, sizeof(int));
      // close the old, unused streams
      close_streama_array(inputstreams + index_to_eat, d);
      // Advance with d
      index_to_eat += d;
      // We have eaten d streams and added one more
      streamsleft -= d-1;

      // close what we got back
      OutputStreamA_close(current);

      // open it again as input, and put it in the back of the array
      InputStreamA *input = InputStreamA_open(buffer, sizeof(int));
      if (input != NULL){
        inputstreams[end_of_array] = input;
      }
      end_of_array++;
    }
    namecounter++;
  }

  free(a);
  free(inputstreams);

  return MERGE_OK;
 error:
  return MERGE_ERR;
}

/*
 * Assumes that N and M are powers of two, i.e that M divides N perfectly!
 *
 *
*/
int mergesort_ext_B(const char *inputfilename, const char *outputfilename, int N, int M, int d)
{
  check( d >= MINIMAL_D, "Invalid d!");


  int number_of_streams = (int) ceil(N/M);
  int i;
  char buffer[PATH_SIZE] = {0};
  int *a = malloc(sizeof(int) * M);
  check_mem(a);

  /* open file */
  InputStreamB *inputstream = InputStreamB_open(inputfilename, sizeof(int), N);
  check(inputstream != NULL, "Failed to open inputstream.");

  /* create streams for each fragment of the file and sort */
  /* Notice the assumptions that M divides N!! */
  for(i = 0; i < number_of_streams; i++)
    {
      InputStreamB_read_next(inputstream,a,M);
      quicksort(a,M);
      next_stream_name(buffer, PATH_SIZE, i, fileprefix_B);
      OutputStreamB *os = OutputStreamB_create(buffer,sizeof(int),M);
      OutputStreamB_write(os,a,M);
      OutputStreamB_close(os);
    }

  InputStreamB_close(inputstream);


  InputStreamB **inputstreams = calloc(number_of_streams*2,sizeof(InputStreamB*));
  check_mem(inputstreams);
  // make M inputstreams for merging, using the sorted files we spawned earlier
  for (i = 0; i < number_of_streams; i++)
    {
      next_stream_name(buffer, PATH_SIZE, i, fileprefix_B);
      InputStreamB *input = InputStreamB_open(buffer, sizeof(int), M);
      check(input != NULL, "input was null, iteration was %d", i);
      inputstreams[i] = input;
    }
  // whoops, we opened N/M streams in total, perhaps crashing the program
  // because of operating system limits on total number of file descriptors open

  int streamsleft = number_of_streams;
  int end_of_array = number_of_streams;
  int index_to_eat = 0;
  int namecounter = 0;
  OutputStreamB *current;
  while(streamsleft > 1)
    {
      // get a fresh fragment name
      next_stream_name(buffer, PATH_SIZE, namecounter+number_of_streams, fileprefix_B);
      // eat d or less streams
      if (streamsleft <= d)
        {
          current = StreamB_merge(outputfilename, inputstreams + index_to_eat, streamsleft, sizeof(int));

          // close old, unused streams
          StreamB_close_input_streams(inputstreams + index_to_eat, streamsleft);

          // close what we got back
          OutputStreamB_close(current);

          fprintf(stderr, "Mergesort B should be done.");

          // We're done, we had less than d streams left, and merged them into one.
          // We don't add a new stream here.
          break;
        }
      // We are guaranteed to have at least 2 streams left after this iteration
      else
        {
          current = StreamB_merge(buffer, inputstreams + index_to_eat, d, sizeof(int));
          size_t current_size = current->num_elements;

          // close the old, unused streams
          StreamB_close_input_streams(inputstreams + index_to_eat, d);
          // Advance with d
          index_to_eat += d;
          // We have eaten d streams and added one more
          streamsleft -= d-1;

          // close what we got back
          OutputStreamB_close(current);

          // open it again as input, and put it in the back of the array
          InputStreamB *input = InputStreamB_open(buffer, sizeof(int), current_size);
          if (input != NULL){
            inputstreams[end_of_array] = input;
          }
          end_of_array++;
        }
      namecounter++;
    }

  free(a);
  free(inputstreams);

  return MERGE_OK;
 error:
  return MERGE_ERR;
}
int mergesort_ext_C(const char *inputfilename, const char *outputfilename, int N, int M, int d, int B)
{

  // D cannot be smaller than 2.
  check( d >= MINIMAL_D , "Invalid d!");

  int number_of_streams = (int) ceil(N/M);
  int i, j, h;
  char buffer[PATH_SIZE] = {0};
  int *a = malloc(sizeof(int) * M);
  check_mem(a);

  // Open file
  InputStreamC *inputstream = InputStreamC_open(inputfilename, sizeof(int), B);
  check(inputstream != NULL, "Could not open InputStream.");

  int buf = 0;
  // Create streams for each fragment of the file and sort them.
  for (i = 0; i < number_of_streams; i++){
    for (j = 0; j < M; j++){
      if (InputStreamC_read_next(inputstream, &buf) == STREAM_ERR){
        break;
      }

      a[j] = buf;

      if (InputStreamC_end_of_stream(inputstream) != FALSE){
        /* EOF is set after last valid read, so we need to increment j once before breaking */
        j++;
        break;
      }

    }
    // sort
    quicksort(a, j);
    // get a new name of a file
    next_stream_name(buffer, PATH_SIZE, i,fileprefix_C);
    OutputStreamC *outputstream = OutputStreamC_create(buffer, sizeof(int), B);
    // write to disk
    for (h = 0; h < j; h++){
      OutputStreamC_write(outputstream, &a[h]);
    }
    // done, close it for use later.
    OutputStreamC_close(outputstream);
  }
  // close inputstream
  InputStreamC_close(inputstream);

  // allocate double space, this should be enough if d > 1
  // we use O(n log_d n) streams for this
  InputStreamC **inputstreams = calloc(number_of_streams*2,sizeof(InputStreamC*));
  check_mem(inputstreams);
  // make M inputstreams for merging, using the sorted files we spawned earlier
  for (i = 0; i < number_of_streams; i++){
    next_stream_name(buffer, PATH_SIZE, i, fileprefix_C);
    InputStreamC *input = InputStreamC_open(buffer, sizeof(int), B);
    check(input != NULL, "input was null, iteration was %d", i);
    inputstreams[i] = input;
  }

  int streamsleft = number_of_streams;
  int end_of_array = number_of_streams;
  int index_to_eat = 0;
  int namecounter = 0;
  OutputStreamC *current;
  while(streamsleft > 1){
    /* fprintf(stderr, "eat index : %d, end_of_array : %d, namecounter : %d, streamsleft : %d, \n" */
    /*         ,index_to_eat, end_of_array, namecounter, streamsleft); */
    // get a fresh fragment name
    next_stream_name(buffer, PATH_SIZE, namecounter+number_of_streams, fileprefix_C);
    // eat d or less streams
    if (streamsleft <= d){
      current = StreamC_merge(outputfilename, inputstreams + index_to_eat, streamsleft, sizeof(int));
      // close old, unused streams
      StreamC_close_input_streams(inputstreams + index_to_eat, streamsleft);

      // close what we got back
      OutputStreamC_close(current);

      fprintf(stderr, "Mergesort C should be done.");

      // We're done, we had less than d streams left, and merged them into one.
      // We don't add a new stream here.
      break;
    }
    // We are guaranteed to have at least 2 streams left after this iteration
    else {
      current = StreamC_merge(buffer, inputstreams + index_to_eat, d, sizeof(int));
      // close the old, unused streams
      StreamC_close_input_streams(inputstreams + index_to_eat, d);
      // Advance with d
      index_to_eat += d;
      // We have eaten d streams and added one more
      streamsleft -= d-1;

      // close what we got back
      OutputStreamC_close(current);

      // open it again as input, and put it in the back of the array
      InputStreamC *input = InputStreamC_open(buffer, sizeof(int), B);
      if (input != NULL){
        inputstreams[end_of_array] = input;
      }
      end_of_array++;
    }
    namecounter++;
  }

  free(a);
  free(inputstreams);

  return MERGE_OK;
 error:
  return MERGE_ERR;
}

int mergesort_ext_D
(const char *inputfilename, const char *outputfilename, int N, int M, int d, int B){
  // D cannot be smaller than 2.
  check( d >= MINIMAL_D , "Invalid d!");

  int number_of_streams = (int) ceil(N/M);
  int i, j, buf = 0;
  // Used for filenames.
  char buffer[PATH_SIZE] = {0};
  int *a = malloc(sizeof(int) * M);
  check_mem(a);

  // Open file.
  InputStreamD *inputstream = InputStreamD_open(inputfilename, B);
  check(inputstream != NULL, "Could not open InputStream.");

  OutputStreamD *fragout;
  // Create sorted fragments of the file.
  for (i = 0; i < number_of_streams; i++){
    // Get some elements.
    for (j = 0; j < M; j++){
      if (InputStreamD_end_of_stream(inputstream) != FALSE){
        break;
      }
      InputStreamD_read_next(inputstream, &buf);
      a[j] = buf;
    }

    // Make new fragment.
    next_stream_name(buffer, PATH_SIZE, i, fileprefix_D);
    fragout = OutputStreamD_create(buffer, B);
    // Sort the elements, and write them.
    quicksort(a, j);
    OutputStreamD_write(fragout, a, j);
    OutputStreamD_close(fragout);
  }

  // Close inputstream.
  InputStreamD_close(inputstream);

  // Allocate double space, this should be enough if d > 1.
  // We use O(n log_d n) streams for this.
  InputStreamD **inputstreams = calloc(number_of_streams * 2, sizeof(InputStreamD*));
  check_mem(inputstreams);

  // Make M inputstreams for merging, using the sorted files we spawned earlier
  for (i = 0; i < number_of_streams; i++){
    next_stream_name(buffer, PATH_SIZE, i, fileprefix_D);
    InputStreamD *input = InputStreamD_open(buffer, B);
    check(input != NULL, "Input was null, iteration was %d", i);
    inputstreams[i] = input;
  }

  int streamsleft = number_of_streams;
  int end_of_array = number_of_streams;
  int index_to_eat = 0;
  int namecounter = 0;
  fprintf(stderr, "Spawned %d streams, gonna process... \n", number_of_streams);
  OutputStreamD *current;
  while(streamsleft > 1){
    // get a fresh fragment name
    next_stream_name(buffer, PATH_SIZE, namecounter+number_of_streams, fileprefix_D);
    // eat d or less streams
    if (streamsleft <= d){
      current = StreamD_merge(outputfilename,
                              inputstreams + index_to_eat,
                              streamsleft,
                              sizeof(int),
                              B);
      // close old, unused streams
      StreamD_close_input_streams(inputstreams + index_to_eat, streamsleft);

      // close what we got back
      OutputStreamD_close(current);

      fprintf(stderr, "Mergesort D should be done.\n");

      // We're done, we had less than d streams left, and merged them into one.
      // We don't add a new stream here.
      break;
    }
    // We are guaranteed to have at least 2 streams left after this iteration
    else {
      current = StreamD_merge(buffer,
                              inputstreams + index_to_eat,
                              d,
                              sizeof(int),
                              B);
      
      // close the old, unused streams
      StreamD_close_input_streams(inputstreams + index_to_eat, d);
      // Advance with d
      index_to_eat += d;
      // We have eaten d streams and added one more
      streamsleft -= d-1;

      // close what we got back
      OutputStreamD_close(current);

      // open it again as input, and put it in the back of the array
      InputStreamD *input = InputStreamD_open(buffer, B);
      if (input != NULL){
        inputstreams[end_of_array] = input;
      }
      end_of_array++;
    }
    namecounter++;
  }

  free(a);
  free(inputstreams);

  return MERGE_OK;
 error:
  return MERGE_ERR;
}

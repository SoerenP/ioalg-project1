#ifndef __MERGESORT_H_
#define __MERGESORT_H_

#define MERGE_OK 0
#define MERGE_ERR -1

#include <stdlib.h>
#include <stdio.h>
#include <IOAlg/stream.h>
#include <IOAlg/StreamA.h>
#include <IOAlg/StreamB.h>
#include <IOAlg/StreamC.h>
#include <IOAlg/StreamD.h>

// Read input file filename
// Split into N/M streams, each of size <= M
// Sort each stream with quicksort.
// Store each stream in a queue.
// 
int mergesort_ext_A(const char *inputfilename, const char *outputfilename, int N, int M, int d);
int mergesort_ext_B(const char *inputfilename, const char *outputfilename, int N, int M, int d);
int mergesort_ext_C(const char *inputfilename, const char *outputfilename, int N, int M, int d, int B);
int mergesort_ext_D(const char *inputfilename, const char *outputfilename, int N, int M, int d, int B);

#endif //__MERGESORT_H_

#include <IOAlg/Quicksort.h>

void quicksort (int *a, int n) {
  int left, right, pivot, temp;
  if (n < 2)
    return;
  // find pivot
  pivot = a[n / 2];
  for (left = 0, right = n - 1;; left++, right--) {
    // while left < pivot
    while (a[left] < pivot)
      left++;
    // pivot < right
    while (pivot < a[right])
      right--;
    if (left >= right)
      break;
    // swap
    temp = a[left];
    a[left] = a[right];
    a[right] = temp;
  }
  // first index to right
  quicksort(a, left);
  // left to last index
  quicksort(a + left, n - left);
}

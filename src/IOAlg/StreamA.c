#include <IOAlg/StreamA.h>
#include <IOAlg/dbg.h>
#include <IOAlg/bool.h>
#include <fcntl.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>

// To read integers, set blocksize to 4 (for 32 bit ints)
OutputStreamA *OutputStreamA_create(const char *filename, size_t element_size)
{
	OutputStreamA *st = malloc(sizeof *st);
	check_mem(st);	
	
	st->filename = filename;
	st->element_size = element_size;
	st->eof = FALSE;

	st->handle = open(filename, O_WRONLY | O_CREAT, S_IXUSR | S_IRUSR | S_IWUSR );
	check(st->handle != STREAM_ERR, "Failed to open file");

	return st;
error:
	return NULL;
}

InputStreamA *InputStreamA_open(const char *filename, size_t element_size)
{
	//open for reading, so file should exist
	InputStreamA *st = malloc(sizeof *st);
	check_mem(st);

	st->filename = filename;
	st->element_size = element_size;

	int rc = open(st->filename, O_RDWR);
	check(rc != STREAM_ERR, "Failed to open file.");
	st->handle = rc;
	st->eof = FALSE;

	return st;
error:
	return NULL;
}

int InputStreamA_close(InputStreamA *st)
{
	check(st->handle != 0, "Stream not opened before close.");

	int rc = close(st->handle);
	check(rc != STREAM_ERR, "Failed to close file.");

	free(st);

	return rc;
error:
	return STREAM_ERR;
}

int OutputStreamA_close(OutputStreamA *st)
{
    check_mem(st);
	check(st->handle != 0, "Stream not opened before close.");

	int rc = close(st->handle);
	check(rc != STREAM_ERR, "Failed to close file.");

	free(st);

	return rc;
error:
	return STREAM_ERR;
}

int InputStreamA_read_next(InputStreamA *st, void *buffer)
{
  check(st != NULL && buffer != NULL, 
	"Recieved NULL pointer");

	int rc = read(st->handle, buffer, st->element_size);
	check(rc != STREAM_ERR, "Failed to read from stream.");
	
	if(rc == 0) st->eof = TRUE;

	return rc;
error:
	return STREAM_ERR;
}

int OutputStreamA_write(OutputStreamA *st, void *buffer)
{
	check(st != NULL && buffer != NULL,
		"Recieved NULL pointer");

	int rc = write(st->handle, buffer, st->element_size);
	check(rc != STREAM_ERR, "Failed to write to stream.");

	return rc;
error:
	return STREAM_ERR;
}

int InputStreamA_end_of_stream(InputStreamA *st)
{
	check(st != NULL, "Recieved NULL pointer.");

	return st->eof;
error:
	return FALSE;
}

void StreamA_write_array(OutputStreamA *st, int *A, size_t size)
{
	unsigned int index = 0;
	for(index = 0; index < size; index++)
	{
		OutputStreamA_write(st,&A[index]);
	}
}

void StreamA_read_array(InputStreamA *st, int *A, size_t size)
{
	unsigned int index = 0;
	for(index = 0; index < size; index++)
	{
		InputStreamA_read_next(st,&A[index]);
	}
}

void StreamA_close_input_streams(InputStreamA **streams, int size)
{
	int index = 0;
	for(index = 0; index < size; index++)
	{
		InputStreamA_close(streams[index]);
	}
}

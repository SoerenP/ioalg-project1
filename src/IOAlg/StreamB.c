#include <IOAlg/StreamB.h>
#include <IOAlg/dbg.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>

/* size_t "should" be unsigned, no reason to check non-negative. */
// Creates an output stream for writing.
OutputStreamB *OutputStreamB_create(const char *filename, size_t element_size, size_t num_elements)
{
	check(filename != NULL, "Recived NULL pointer.");
	check((element_size > 0) && (num_elements > 0), "Arguments must be positive.");

	OutputStreamB *st = malloc(sizeof *st);
	check_mem(st);	

	st->filename = filename;
	st->element_size = element_size;
	st->num_elements = num_elements;

	st->file_ptr = fopen(st->filename, "w");
	check(st->file_ptr != NULL, "Failed to open stream.");
	
	return st;
error:
	return NULL;
}

// Writes contents of ptr to the file defined by file_ptr in the struct.
int OutputStreamB_write(OutputStreamB *st, const void *ptr, size_t num_elements)
{
	check((st != NULL) && (ptr != NULL), "Recieved NULL pointers.");

	size_t rc = fwrite(ptr,st->element_size, num_elements, st->file_ptr);
	check(num_elements == rc, "Failed to write correct amount.");

	return rc;
error:
	return STREAM_ERR;
}

// Closes the output stream
int OutputStreamB_close(OutputStreamB *st)
{
	check(st != NULL && st->file_ptr != NULL, "Recieved NULL pointer");

	size_t rc = fclose(st->file_ptr);
	check(rc == 0, "Failed to close stream.");

    free(st);
    
	return STREAM_OK;
error: 	
	return STREAM_ERR;
}

// Opens stream for input, such that we can read
InputStreamB *InputStreamB_open(const char *filename, size_t element_size, size_t num_elements)
{
  	check(filename != NULL, "Recived NULL pointer.");
	check((element_size > 0) && (num_elements > 0), "Arguments must be positive.");

	InputStreamB *st = malloc(sizeof *st);
	check_mem(st);	

	st->filename = filename;
	st->element_size = element_size;
	st->num_elements = num_elements;

	st->file_ptr = fopen(st->filename, "r");
	check(st->file_ptr != NULL, "Failed to open stream.");
	
	return st;
error:
	return NULL;
}

// Attempts to read the next element from the file
int InputStreamB_read_next(InputStreamB *st, void *ptr, size_t num_elements)
{
	check((st != NULL) && (ptr != NULL), "Recieved NULL pointers.");

	size_t rc = fread(ptr, st->element_size, num_elements, st->file_ptr);
	//check(num_elements == rc, "Failed to read correct amount.");
	if(num_elements != rc) goto error;

	return rc;
error:
	return STREAM_ERR;
}

// Returns wether or not we are the end of the file
int InputStreamB_end_of_stream(InputStreamB *st)
{
	check((st != NULL) && (st->file_ptr != NULL), "Recieved NULL pointer.");

	if(feof(st->file_ptr)){
		return 1;
	} else {
		return 0;
	} 

error: //fall through
	return STREAM_ERR;	
}

// Closes the input stream
int InputStreamB_close(InputStreamB *st)
{
	check(st != NULL && st->file_ptr != NULL, "Recieved NULL pointer");

	size_t rc = fclose(st->file_ptr);
	check(rc == 0, "Failed to close stream.");

    free(st);
    
	return STREAM_OK;
error: 	
	return STREAM_ERR;
}

void StreamB_close_input_streams(InputStreamB **streams, int size)
{
	int index = 0;
	for(index = 0; index < size; index++)
	{
		InputStreamB_close(streams[index]);
	}
}

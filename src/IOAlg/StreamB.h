#ifndef IOAlg_StreamB_h
#define IOAlg_StreamB_h

#include <stddef.h>
#include <stdio.h>
#include <IOAlg/stream.h>

typedef struct InputStreamB {
	FILE *file_ptr;
	const char *filename;
	size_t element_size;
	size_t num_elements;
} InputStreamB;

typedef struct OutputStreamB {
	FILE *file_ptr;
	const char *filename;
	size_t element_size;
	size_t num_elements;
} OutputStreamB;

OutputStreamB *OutputStreamB_create(const char *filename, size_t element_size, size_t num_elements);
int OutputStreamB_write(OutputStreamB *st, const void *ptr, size_t num_elements);
int OutputStreamB_close(OutputStreamB *st);

InputStreamB *InputStreamB_open(const char *filename, size_t element_size, size_t num_elements);
int InputStreamB_read_next(InputStreamB *st, void *ptr, size_t num_elements);
int InputStreamB_end_of_stream(InputStreamB *st);
int InputStreamB_close(InputStreamB *st);
void StreamB_close_input_streams(InputStreamB **streams, int size);
#endif

#include <IOAlg/StreamC.h>
#include <IOAlg/dbg.h>
#include <IOAlg/bool.h>
#include <fcntl.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

OutputStreamC *OutputStreamC_create
(const char *filename, size_t element_size, size_t buffer_size)
{
	OutputStreamC *st = malloc(sizeof *st);
	check_mem(st);

	check((filename != NULL) &&
		(element_size > 0) && 
		(buffer_size > 0),
		"Recieved invalid parameters for stream.");

	st->buffer = malloc(element_size * buffer_size);
	check_mem(st->buffer);

	st->handle = open(filename, O_WRONLY | O_CREAT, S_IXUSR | S_IRUSR | S_IWUSR);
	check(st->handle != STREAM_ERR, "Failed to open file.");

	st->index = 0; //start from beginning
	st->filename = filename;
	st->element_size = element_size;
	st->buffer_size = buffer_size;
	return st;
error:
	if(st->buffer) free(st->buffer);
	if(st) free(st);

	return NULL;
}

int InputStreamC_end_of_stream(InputStreamC *st)
{
	return st->eof;
}

/* Called when stream buffer is full, dump it to the file */
static int StreamC_buffer_to_file(OutputStreamC *st, int num_elements)
{
	//Just dump things into the file (append!)
	int rc = 0;

	rc = write(st->handle,st->buffer, num_elements * st->element_size);
	check(rc >= 0, "Failed to write entire buffer.");

	//Set the index of the stream to be the front of the buffer
	memset(st->buffer, 0, st->buffer_size);
	st->index = 0; //the buffer should write into the start since we dumped it.

	return rc;
error:
	return STREAM_ERR;
}

/* Called whe the stream buffer is empty and we want to read */
/* so load file into buffer */
static int StreamC_file_to_buffer(InputStreamC *st, int num_elements)
{
	int rc = 0;	
	rc = read(st->handle, st->buffer, num_elements * st->element_size);
	//rc == 0 means EOF, we allow this ofc :)
	check(rc >= 0, "Failed to read into entire buffer.");
	st->index = 0; //Reading should begin from the start. 
	st->elements_in_buffer = rc / st->element_size; //rc is bytes read, and each element takes up blocksize bytes. 
	
	if(rc <= 0 ){
		st->eof = TRUE;
	} else {
		st->eof = FALSE;
	}
	
	return rc;
error:
	return STREAM_ERR;
}

/* open MUST be called before reading! To ensure there is something in the buffer */
InputStreamC *InputStreamC_open
(const char *filename, size_t element_size, size_t buffer_size)
{
	InputStreamC *st = malloc(sizeof *st);
	check_mem(st);
	
	check((filename != NULL) &&
		(element_size > 0) && 
		(buffer_size > 0),
		"Recieved invalid parameters for stream.");

	st->buffer = malloc(element_size *  buffer_size);
	check_mem(st->buffer);
	
	st->filename = filename;
		
	int rc = open(st->filename, O_RDWR);
	check(rc != STREAM_ERR, "Failed to open file.");
	st->handle = rc;

	st->eof = FALSE;
	st->index = 0;
	st->elements_in_buffer = 0;
	st->element_size = element_size;
	st->buffer_size = buffer_size;

	//We should read something into the buffer from the get-go.
	if(st->index == 0){
		StreamC_file_to_buffer(st, st->buffer_size);
	}

	return st;
error:
	if(st) free(st);
	if(st->buffer) free(st->buffer);
	return NULL;;
}

/* close MUST be called after writing! To ensure the buffer is dumped! */
int OutputStreamC_close(OutputStreamC *st)
{
	//If buffer still has elements before we close it, write the buffer
	if(st->index > 0) StreamC_buffer_to_file(st, st->index);

	check(st->handle != 0, "Stream not opened before close.");

	int rc = close(st->handle);
	check(rc != STREAM_ERR, "Failed to close file.");
	
	if(st->buffer) free(st->buffer);
	if(st) free(st);

	return rc;
error:
	return STREAM_ERR;
}

int InputStreamC_close(InputStreamC *st)
{
	int rc = close(st->handle);
	check(rc != STREAM_ERR, "Failed to close file.");

	if(st->buffer) free(st->buffer);
	if(st) free(st);

	return rc;
error:
	return STREAM_ERR;
}

int OutputStreamC_write(OutputStreamC *st, void *buffer)
{
	check(st != NULL && buffer != NULL,
		"Recieved NULL pointer.");

	int rc = 0;

	//If index is at end, we need to dump the buffer
	if(st->index == st->buffer_size){
		rc =  StreamC_buffer_to_file(st,st->buffer_size);
		check(rc != STREAM_ERR, 
		"Failed to dump buffer to file.");
	}
	//Then we just copy the data over into the buffer
	memcpy(&st->buffer[st->index],buffer,st->element_size);
	st->index++;

	return st->element_size;
error:
	return STREAM_ERR;
}

int InputStreamC_read_next(InputStreamC *st, void *buffer)
{
	check(st != NULL && buffer != NULL,
		"Recieved NULL pointers.");

	int rc = 0;
	
	//Then we just return the next index in the buffer.
	memcpy(buffer,&st->buffer[st->index],st->element_size);
	st->index++;

	if(st->index == st->buffer_size || st->index == st->elements_in_buffer){
		st->eof = TRUE; //we might be EOF. if we are not, file_to_buffer call will set us to FALSE again. But if we are, we need to set it before next read!
		rc = StreamC_file_to_buffer(st,st->buffer_size);
		check(rc != STREAM_ERR, "Failed to read file into buffer.");
	}
	
	return st->element_size;
error:
	return STREAM_ERR;
}

void StreamC_write_array(OutputStreamC *st, int *A, size_t size)
{
	unsigned int index = 0;
	for(index = 0; index < size; index++)
	{
		OutputStreamC_write(st,&A[index]);
	}
}

void StreamC_read_array(InputStreamC *st, int *A, size_t size)
{
	unsigned int index = 0;
	for(index = 0; index < size; index++)
	{
		InputStreamC_read_next(st,&A[index]);
	}
}

void StreamC_close_input_streams(InputStreamC **streams, int size)
{
	int index = 0;
	for(index = 0; index < size; index++)
	{
		if(streams[index] != NULL) InputStreamC_close(streams[index]);
	}
}

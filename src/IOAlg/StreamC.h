#ifndef IOAlg_streamc_h
#define IOAlg_streamc_h

#include <fcntl.h>
#include <IOAlg/stream.h>
#include <stddef.h>

/* should read elements one at a time using syscalls read(), write() */
/* but should have an internal buffer, as to read B elements at a time */
typedef struct OutputStreamC {
	const char *filename;
	int handle;
	size_t index;
	size_t element_size;
	size_t buffer_size;
	int *buffer;
} OutputStreamC;

typedef struct InputStreamC {
	const char *filename;
	int handle;
	size_t index;
	size_t element_size;
	size_t buffer_size;
	size_t elements_in_buffer;
	int *buffer;
	unsigned int eof : 1;
} InputStreamC;


OutputStreamC *OutputStreamC_create
(const char *filename, size_t element_size, size_t buffer_size);

InputStreamC *InputStreamC_open
(const char *filename, size_t element_size, size_t buffer_size);

int InputStreamC_end_of_stream(InputStreamC *st);

int InputStreamC_close(InputStreamC *st);

int OutputStreamC_close(OutputStreamC *st);

int OutputStreamC_write(OutputStreamC *st, void *buffer);

int InputStreamC_read_next(InputStreamC *st, void *buffer);

void StreamC_write_array(OutputStreamC *st, int *A, size_t size);

void StreamC_read_array(InputStreamC *st, int *A, size_t size);

void StreamC_close_input_streams(InputStreamC **streams, int size);

#endif

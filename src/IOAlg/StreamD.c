/* 
  Currently we mmap each time we have used a pagesize,
  in some other examples we have seen mmapping the entire file in one go.
  Ah. This makes sense, if we use a mmap region of filesize!
  Hmmm.
 */
#include <IOAlg/StreamD.h>
#include <IOAlg/bool.h>
#include <IOAlg/dbg.h>

#include <fcntl.h> //files
#include <stdlib.h>
#include <assert.h>

#include <unistd.h> // open, read, etc
#include <stddef.h> // size_t
#include <sys/stat.h> //page size
#include <sys/mman.h> //mmap

InputStreamD *InputStreamD_open(const char *filename, size_t blocksize){
  if (blocksize <= 0)
    blocksize = getpagesize();

  InputStreamD *st = malloc(sizeof(*st));
  check_mem(st);

  // initialize struct with default things
  st->filename = filename;
  st->blocksize = blocksize;
  st->elementsize = sizeof(int);
  st->offset = 0;
  st->currentpos = 0;
  st->currentmap = NULL;
  struct stat fstat;
  check(stat(filename, &fstat) >= 0, "Error reading filesize of file. Filename was %s.", st->filename);
  st->filesize = fstat.st_size;

  st->fd = open(filename, O_RDWR,  S_IRWXU | S_IRWXG | S_IRWXO);
  check(st->fd != -1, "Error opening file for reading");
  
  // figure out how much more "offset" we need
  int offset = st->blocksize - (st->filesize % st->blocksize);
  
  // stretch the file to match page sizes, such that we can read
  // -1, because we want to be just before the end
  int eof = lseek(st->fd, st->filesize + offset - 1, SEEK_SET);
  check(eof != -1, "Could not go to end of file.");

  eof = write(st->fd, "", 1);
  check(eof == 1, "Could not write to the end of the file.");

  // go to the start of the file again.
  lseek(st->fd, 0, SEEK_SET);

  return st;
 error:
  return NULL;
}


void readInNewMap_i(InputStreamD *st){
  size_t length = st->blocksize;
  size_t offset = st->offset * length;
  st->currentmap = (int*) mmap(0, length, PROT_READ, MAP_SHARED, st->fd, offset);
  check(st->currentmap != MAP_FAILED, "Could not map the memory. ");
 error:
  return;
}

void releaseMap_i(InputStreamD *st){
  size_t length = st->blocksize;
  check(munmap(st->currentmap, length) != -1, "Could not unmap the memory.");
  st->currentmap = NULL;
  st->offset++; // count up the offset if we are to read in more stuff
 error:
  return;
}

int InputStreamD_read_next(InputStreamD *st, void *ptr){
  int elements = st->blocksize / st->elementsize; // 1024
  // Our current position in the page
  unsigned int pos_map = st->currentpos % elements;
  
  // Reached end of block, or start of map
  if (pos_map == 0){
    // End of block
    if (st->currentmap != NULL)
      releaseMap_i(st);
      
    readInNewMap_i(st);
  }
  
  memcpy(ptr, &(st->currentmap[pos_map]), st->elementsize);

  st->currentpos++;
  
  return st->elementsize;
}

// Dirty fix: we fill everything with 0's in the end of the file, if it has been
// stretched. So if we meet a 0, let's stop!
int InputStreamD_end_of_stream(InputStreamD *st){
  unsigned int pos_map = st->currentpos % (st->blocksize / st->elementsize);

  if (st->currentpos == 0) return FALSE; // We just started
  if (st->currentmap != NULL && st->currentmap[pos_map] == 0)
    { return TRUE; } // Dirty hack
  if (st->currentmap == NULL)
    { return TRUE; }// We don't have a map to read.
    
  // do we exceed the file? 
  if (st->currentpos >= (unsigned int) (st->filesize / st->elementsize)){
    return TRUE;
  }
  
  return FALSE;
}

int InputStreamD_close(InputStreamD *st){
  releaseMap_i(st);
  check(close(st->fd) != -1, "Could not close InputStreamD");
  free(st);
  
  return STREAM_OK;
 error:
  return STREAM_ERR;
}

OutputStreamD *OutputStreamD_create(const char *filename, size_t blocksize){
  if (blocksize <= 0)
    blocksize = getpagesize();

  OutputStreamD *st = malloc(sizeof(*st));
  check_mem(st);

  // initialize struct with default things
  st->filename = filename;
  st->blocksize = blocksize;
  st->elementsize = sizeof(int);
  st->offset = 0; // offset in file
  st->currentmap = NULL;
  st->currentpos = 0; // current position in the map
  
  st->fd = open(filename,
                O_RDWR | O_CREAT | O_TRUNC,
                S_IRWXU | S_IRWXG | S_IRWXO );
  check(st->fd != -1, "Error opening file for reading");
    
  return st;
 error:
  return NULL;
}

int readInNewMap_o(OutputStreamD *st){
  size_t length = st->blocksize;
  size_t offset = st->offset * length;
  st->currentmap = (int*) mmap(0, length, PROT_READ | PROT_WRITE, MAP_SHARED, st->fd, offset);
  check(st->currentmap != MAP_FAILED, "Could not map the memory. ");
  return STREAM_OK;
 error:
  return STREAM_ERR;
}

int releaseMap_o(OutputStreamD *st){
  size_t length = st->blocksize;
  check(munmap(st->currentmap, length) != -1, "Could not unmap the memory.");
  st->currentmap = NULL;
  st->offset++; // count up the offset if we are to read in more stuff
  return STREAM_OK;
 error:
  return STREAM_ERR;
}

int OutputStreamD_write(OutputStreamD *st, int *ptr, size_t size){
  // I want to calculate in bytes.
  // So size of array is transformed into the bytes by using the element size.
  // Blocksize should be in bytes already
  st->filesize = size*st->elementsize;
  
  // figure out how much more "offset" we need
  int offset = st->blocksize - (st->filesize % st->blocksize);

  // expand the file to contain our data
  check(ftruncate(st->fd, (st->filesize + offset)) != -1, "could not truncate");
  
  // stretch the file to match page sizes, such that we can write
  // -1, because we want to be just before the end
  int eof = lseek(st->fd, (st->filesize + offset) - 1, SEEK_SET);
  check(eof != -1, "Could not go to end of file.");
  
  eof = write(st->fd, "", 1);
  check(eof == 1, "Could not write to the end of the file.");

  // go to the start of the file again.
  lseek(st->fd, 0, SEEK_SET);
  
  // first map
  readInNewMap_o(st);
 
  // Time to map the file. We can fit in blocksize bytes, but we
  // use elementsize for each index, meaning the total assignments that
  // can be done is blocksize/elementsize
  while (st->currentpos < size) {
    // If we exceed the map, write it and map a new one!
    if (st->currentpos > (st->offset+1) * (st->blocksize / st->elementsize)-1){
      // fprintf(stderr, "Reading in new map, since currentpos: %d \n", st->currentpos);
      int err = msync(st->currentmap, st->blocksize, MS_SYNC);
      check(err != -1, "could not write from map to disk");
      
      if (st->currentmap != NULL){
        releaseMap_o(st);
      }
      
      readInNewMap_o(st);
    }
    // fprintf(stderr, "currentpos: %d \n" ,st->currentpos);
    st->currentmap[st->currentpos % (st->blocksize / st->elementsize)] = ptr[st->currentpos];
    st->currentpos++;
  }

  // Write the last bit.
  int err = msync(st->currentmap, (st->currentpos * st->elementsize) % st->blocksize, MS_SYNC);
  check(err != -1, "could not write from map to disk");
  releaseMap_o(st);
  
  return STREAM_OK;
 error:
  return STREAM_ERR;
}

int OutputStreamD_close(OutputStreamD *st){
  if (st->currentmap != NULL)
    releaseMap_o(st);
  int rc = close(st->fd);
  check(rc != -1, "could not close file");
  free(st);
  return STREAM_OK;
 error:
  return STREAM_ERR;
}

void StreamD_read_array(InputStreamD *st, int *A)
{
  int index = 0;
  while (InputStreamD_end_of_stream(st) == FALSE){
    InputStreamD_read_next(st, &A[index]);
    index++;
  }
}

void StreamD_close_input_streams(InputStreamD **inputstreams, int d){
  int index = 0;
  for (index = 0; index < d; index++){
    if (inputstreams[index] != NULL){
      InputStreamD_close(inputstreams[index]);
    }
  }
}
     

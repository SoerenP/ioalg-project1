#ifndef IOAlg_StreamD_h
#define IOAlg_StreamD_h

#include <stddef.h>
#include <stdio.h>
#include <IOAlg/stream.h>

typedef struct InputStreamD {
  const char *filename;
  size_t blocksize; // size of page / B
  size_t elementsize; // size of each element we read

  int fd; // file descriptor
  off_t filesize; // the actual filesize 
  int* currentmap; // our current memory mapped block
  int offset; // our current block of the file, used for mmap
  size_t currentpos;
  // our current position in the file, to avoid reading bogus
  // of a stretched file
} InputStreamD;

typedef struct OutputStreamD {
  const char *filename;
  size_t blocksize; // size of page / B
  size_t elementsize; // size of each element we read

  int fd; // file descriptor
  off_t filesize; // the actual filesize
  int* currentmap; // our current memory mapped block
  size_t currentpos;
  int offset; // our current block of the file
} OutputStreamD;

OutputStreamD *OutputStreamD_create(const char *filename, size_t blocksize);
int OutputStreamD_write(OutputStreamD *st, int *ptr, size_t size);
int OutputStreamD_close(OutputStreamD *st);

InputStreamD *InputStreamD_open(const char *filename, size_t blocksize);
int InputStreamD_read_next(InputStreamD *st, void *ptr);
int InputStreamD_end_of_stream(InputStreamD *st);
int InputStreamD_close(InputStreamD *st);
void StreamD_read_array(InputStreamD *st, int *A);
void StreamD_write_array(OutputStreamD *st, int *A, int size);
void StreamD_close_input_streams(InputStreamD **inputstreams, int d);
#endif

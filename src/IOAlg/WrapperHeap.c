#include <IOAlg/WrapperHeap.h>
#include <IOAlg/dbg.h>
#include <stdlib.h>

static int double_the_space(WrapperHeap *Q){
  Wrapper *temp = realloc(Q->elements, 2*Q->maxsize*sizeof(Wrapper));
  check_mem(temp);

  Q->elements = temp;
  Q->maxsize = Q->maxsize*2;

  return WRAPPER_HEAP_TRUE;
 error:
  return WRAPPER_HEAP_FALSE;
}

static int isFull(WrapperHeap *Q){
  if (Q->idx >= Q->maxsize)
    return WRAPPER_HEAP_TRUE;
  return WRAPPER_HEAP_FALSE;
}

static int isEmpty(WrapperHeap *Q){
  if (Q->idx <= 0)
    return WRAPPER_HEAP_TRUE;
  return WRAPPER_HEAP_FALSE;
}

static void swap(WrapperHeap *Q, int i, int j) {
  Wrapper temp = Q->elements[i];
  Q->elements[i] = Q->elements[j];
  Q->elements[j] = temp;
}


// Heap implementation
WrapperHeap* wrapper_heap_makeHeap() {
  WrapperHeap *Q = malloc(sizeof(*Q));
  check_mem(Q);
  Q->idx = 0;
  Q->maxsize = 128;
  Q->elements = malloc(Q->maxsize * sizeof(Wrapper));
  check_mem(Q->elements);

  return Q;
 error:
  return NULL;
}

Wrapper wrapper_heap_makeWrapper(int key, int list){
  Wrapper w;
  w.key = key;
  w.list = list;
  return w;
}

void wrapper_heap_destroy(WrapperHeap *Q){
  free(Q->elements);
  free(Q);
}

int wrapper_heap_insert(WrapperHeap *Q, Wrapper w) {
  if (isFull(Q) == WRAPPER_HEAP_TRUE){
    check(double_the_space(Q) == WRAPPER_HEAP_TRUE, "Could not double the space of array.");
  }
  Q->elements[Q->idx] = w;
  Q->idx++;

  int inserted = Q->idx - 1;
  int indexOfParent = (inserted + 1) / 2 - 1;
  while (0 < inserted &&
         Q->elements[inserted].key < Q->elements[indexOfParent].key) {
    swap(Q, inserted, indexOfParent);
    inserted = indexOfParent;
    indexOfParent = (inserted + 1) / 2 - 1;
  }
  return WRAPPER_HEAP_TRUE;
  
 error:
  return WRAPPER_HEAP_FALSE;
}

Wrapper wrapper_heap_deleteMin(WrapperHeap *Q) {
  if (isEmpty(Q) == WRAPPER_HEAP_TRUE){
    Wrapper w;
    w.key = -1;
    w.list = -1;
    return w;
  }

  Wrapper ret = Q->elements[0];
  swap(Q, 0, Q->idx - 1);
  Q->idx--;

  int size = Q->idx;
  int nodeToBeFixed = 0;
  int childL = (nodeToBeFixed + 1) * 2 - 1;
  int childR = (nodeToBeFixed + 1) * 2;

  while (1) {
    // If nodeToBeFixed doesn't have any children.
    if (size <= childL) break;

    // If nodeToBeFixed only has a left child.
    if (size == childR) {
      if (Q->elements[childL].key < Q->elements[nodeToBeFixed].key){
        swap(Q,nodeToBeFixed, childL);
      }
      break;
    }

    // If nodeToBeFixed has both children.
    // Find the smallest of the two children
    if (Q->elements[childL].key < Q->elements[childR].key &&
        Q->elements[childL].key < Q->elements[nodeToBeFixed].key) {
      swap(Q, nodeToBeFixed, childL);
      nodeToBeFixed = childL;
      childL = (nodeToBeFixed + 1) * 2 - 1;
      childR = (nodeToBeFixed + 1) * 2;
    }
    // We know that the left child couldn't have been the smallest node of the three.
    // So, we test whether the right child is smaller than nodeToBeFixed.
    else if (Q->elements[childR].key < Q->elements[nodeToBeFixed].key) {
      swap(Q, nodeToBeFixed, childR);
      nodeToBeFixed = childR;
      childL = (nodeToBeFixed + 1) * 2 - 1;
      childR = (nodeToBeFixed + 1) * 2;
    } else {
      break;
    }
  }

  return ret;
}

/* overrides the input array to be sorted */
void wrapper_heapsort_inplace(Wrapper *A, int size)
{
  WrapperHeap *H = wrapper_heap_makeHeap();
  check_mem(H);

  int index = 0;
  for(index = 0; index < size; index++)
	{
      wrapper_heap_insert(H, A[index]);
	}

  for(index = 0; index < size; index++)
	{
      A[index] = wrapper_heap_deleteMin(H);
	}

  if(H) wrapper_heap_destroy(H);
 error:
  return;
}

Wrapper *wrapper_heapsort(Wrapper *A, int size)
{
  WrapperHeap *H = wrapper_heap_makeHeap();
  check_mem(H);

  Wrapper *sorted = malloc(sizeof(Wrapper) * size);
  check_mem(sorted);

  int index = 0;
  for(index = 0; index < size; index++)
	{
      wrapper_heap_insert(H,A[index]);
	}

  for(index = 0; index < size; index++)
	{
      sorted[index] = wrapper_heap_deleteMin(H);
	}

  if(H) wrapper_heap_destroy(H);
  return sorted;
 error:
  if(H) wrapper_heap_destroy(H);
  return NULL;
}

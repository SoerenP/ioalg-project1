#ifndef __WRAPPERHEAP_H_
#define __WRAPPERHEAP_H_

typedef struct Wrapper {
  int key; // key of the thing we insert
  int list; // list number where it came from
} Wrapper;

typedef struct WrapperHeap {
  int idx;
  Wrapper* elements;
  int maxsize;
} WrapperHeap;

#define WRAPPER_HEAP_TRUE 0
#define WRAPPER_HEAP_FALSE (-1)

/// PUBLIC
WrapperHeap *wrapper_heap_makeHeap();
Wrapper wrapper_heap_makeWrapper(int key, int list);

int wrapper_heap_insert(WrapperHeap *Q, Wrapper w);
Wrapper wrapper_heap_deleteMin(WrapperHeap *Q);
void wrapper_heap_destroy(WrapperHeap *Q);

Wrapper *wrapper_heapsort(Wrapper *A, int size);
void wrapper_heapsort_inplace(Wrapper *A, int size);

#endif // __WRAPPERHEAP_H_

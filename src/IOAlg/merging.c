#include <IOAlg/merging.h>
#include <IOAlg/Heap.h>
#include <IOAlg/WrapperHeap.h>
#include <IOAlg/dbg.h>
#include <IOAlg/stream.h>
#include <IOAlg/bool.h>

static int StreamB_sum_elements(InputStreamB **st, int amount)
{
  check(st != NULL, "Recieved NULL pointer.");

  int sum = 0;
  int index = 0;
  for(index = 0; index < amount; index++)
    {
      sum += st[index]->num_elements;
    }

  return sum;
 error:
  return STREAM_ERR;
}

OutputStreamA *StreamA_merge(const char *filename, InputStreamA **sorted_streams, int d, size_t element_size)
{
  Heap *H = NULL;
  check(sorted_streams != NULL && filename != NULL, "Recieved NULL pointer.");

  /* insert from all streams until EOF into the heap */
  H = heap_makeHeap();
  check_mem(H);

  int buffer = HEAP_TRUE;

  /* insert into buffer until each stream says EOF */
  int index;
  for(index = 0; index < d; index++)
    {
      while(1)
        {
          InputStreamA_read_next(sorted_streams[index], &buffer);

          if (InputStreamA_end_of_stream(sorted_streams[index]) != FALSE){
            break;
          }
          heap_insert(H, buffer);
        }
    }
  /* create output stream */
  OutputStreamA *res = OutputStreamA_create(filename, element_size);
  check_mem(res);

  buffer = heap_deleteMin(H);
  /* repeatedly extract min from buffer */
  while(buffer != HEAP_FALSE)
    {
      OutputStreamA_write(res, &buffer);
      buffer = heap_deleteMin(H);
    }

  /* all done, cleanup before returning */

  heap_destroy(H);

  return res;
 error:
  if(H != NULL) heap_destroy(H);

  return NULL;
}

/* assumes array of input streams are sorted internally, outputs outputstream with all elements sorted using heasort */
OutputStreamB *StreamB_merge
(const char *filename, InputStreamB **sorted_streams, int d, size_t element_size)
{
  WrapperHeap *H = NULL;

  check(sorted_streams != NULL, "Recieved NULL pointer.");

  int num_elements = StreamB_sum_elements(sorted_streams, d);
  OutputStreamB *res = OutputStreamB_create(filename, element_size, num_elements);
  check(res != NULL, "Failed to create Input Stream.");

  /* heap stuff */
  H = wrapper_heap_makeHeap();
  int index = 0;
  int buf;
  // take an element from each stream and put it into the heap.
  for (index = 0; index < d; index++) {
      InputStreamB_read_next(sorted_streams[index], &buf, 1);
      Wrapper temp = wrapper_heap_makeWrapper(buf, index);
      wrapper_heap_insert(H, temp);
  }

  int end = 0;
  while(1){
    Wrapper temp = wrapper_heap_deleteMin(H);
    if (temp.key == -1) break;
    
    OutputStreamB_write(res, &temp.key, 1);
    
    int rc = InputStreamB_read_next(sorted_streams[temp.list], &buf, 1);
    if (rc != STREAM_ERR){      
      Wrapper newelement = wrapper_heap_makeWrapper(buf, temp.list);
      wrapper_heap_insert(H, newelement);
    } else {
      end++;
      if (end == d){
        break;
      } // Goodbye, we are done
      // Try another list.
    }
  }


  wrapper_heap_destroy(H);
  return res;
 error:
  if(H != NULL) wrapper_heap_destroy(H);
  return NULL;
}


OutputStreamC *StreamC_merge(const char *filename, InputStreamC **sorted_streams, int d, size_t element_size)
{
  Heap *H = NULL;
  check((filename != NULL) && (sorted_streams != NULL) && (d > 0) && (element_size > 0),
        "Recieved invalid parameters for merge");

  H = heap_makeHeap();
  check_mem(H);

  int buffer;

  /* insert into buffer until each stream says EOF */
  int index = 0;
  for(index = 0; index < d; index++)
    {
      while(TRUE){
        InputStreamC_read_next(sorted_streams[index],&buffer);
        heap_insert(H,buffer);
        if(InputStreamC_end_of_stream(sorted_streams[index]) != FALSE){
          break;
        }
      }
    }

  /* create output stream */
  OutputStreamC *res = OutputStreamC_create(
                                            filename,
                                            element_size,
                                            sorted_streams[0]->buffer_size);
  check_mem(res);

  buffer = heap_deleteMin(H);
  /* repeatedly extract min from buffer */
  while(buffer != HEAP_FALSE)
    {
      OutputStreamC_write(res, &buffer);
      buffer = heap_deleteMin(H);
    }

  /* all done, cleanup before returning */

  heap_destroy(H);

  return res;
 error:
  if(H != NULL) heap_destroy(H);
  return NULL;
}

OutputStreamD *StreamD_merge(const char *filename, InputStreamD **sorted_streams, int d, size_t element_size, size_t buffer_size){
  Heap *H = NULL;
  H = heap_makeHeap();
  check_mem(H);

  OutputStreamD *res = NULL;

  // Combine streams into a heap
  int buffer = 0;
  int index = 0;
  for(index = 0; index < d; index++){
    while (InputStreamD_end_of_stream(sorted_streams[index]) == FALSE){
      InputStreamD_read_next(sorted_streams[index], &buffer);
      heap_insert(H, buffer);
    }
  }

  // At this point we have a large heap in the memory.
  int arrsize = H->idx;
  int *sorted_array = malloc((arrsize+1) * element_size);

  res = OutputStreamD_create(filename, buffer_size);
  check_mem(res);

  buffer = heap_deleteMin(H);
  index = 0;
  // Sort to a temporary array before writing. 
  while(buffer != HEAP_FALSE){
    sorted_array[index] = buffer;
    buffer = heap_deleteMin(H);
    index++;
  }

  OutputStreamD_write(res, sorted_array, arrsize);
  
  heap_destroy(H);
  free(sorted_array);

  return res;
 error:
  return NULL;
}

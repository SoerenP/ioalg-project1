#ifndef _merging_h_
#define _merging_h_

#include <stdlib.h>
#include <stdio.h>
#include <IOAlg/stream.h>
#include <IOAlg/StreamA.h>
#include <IOAlg/StreamB.h>
#include <IOAlg/StreamC.h>
#include <IOAlg/StreamD.h>

/*
 * All functions assume that input streams are sorted
 *
 *
 */
OutputStreamA *StreamA_merge(const char *filename, InputStreamA **sorted_streams, int d, size_t element_size);

OutputStreamB *StreamB_merge(const char *filename, InputStreamB **sorted_streams, int d, size_t element_size);

OutputStreamC *StreamC_merge(const char *filename, InputStreamC **sorted_streams, int d, size_t element_size);

OutputStreamD *StreamD_merge(const char *filename, InputStreamD **sorted_streams, int d, size_t element_size, size_t buffer);

#endif

#include <IOAlg/Heap.h>
#include <IOAlg/minunit.h>
#include <IOAlg/dbg.h>
#include <IOAlg/bool.h>
#include <time.h>
#include <stdlib.h>

#define TEST_SIZE 256
#define BLOCK_SIZE sizeof(int)

static Heap *Q;
static int test_array[TEST_SIZE] = {0};

void test_array_setup()
{
	int index = 0;
	for(index = 0; index < TEST_SIZE; index++)
	{
		test_array[index] = rand();
	}
}

char *test_destroy(){
  heap_destroy(Q);
  return NULL;
}

char *makeheap()
{
  Q = heap_makeHeap();
  mu_assert(NULL != Q, "Failed to make heap, fuck..");

  return NULL;
}

char *insertion(){
  int index;
  for (index = 0; index < TEST_SIZE; index++){
    mu_assert(HEAP_TRUE == heap_insert(Q, test_array[index]), "could not insert into heap");
  }

  return NULL;
}

char *delmin(){
  int index;
  for (index = 0; index < TEST_SIZE; index++){
    test_array[index] = heap_deleteMin(Q);
  }

  for (index = 0; index < TEST_SIZE-1; index++){
    mu_assert(test_array[index] <= test_array[index+1], "array was not sorted");
  }
  return NULL;
}

char *test_heapsort_inplace()
{
	test_array_setup();

	heapsort_inplace(test_array, TEST_SIZE);

	int index = 0;
	for(index = 0; index < TEST_SIZE-1; index++)
	{
		mu_assert(test_array[index] <= test_array[index+1], "Array was not sorted");
	}

	return NULL;
}

char *test_heapsort()
{
	test_array_setup();

	int *sorted = heapsort(test_array, TEST_SIZE);
	mu_assert(sorted != NULL, "Failed to return array from heapsort.");

	int index = 0;
	for(index = 0; index < TEST_SIZE-1; index++)
	{
		mu_assert(sorted[index] <= sorted[index+1], "Array was not sorted");
	}	

	free(sorted);

	return NULL;
}



/* tests are interdependant and should run in the given order */
/* not pretty, consider using seperate stream structs pr test */
char *all_tests()
{
	srand(time(NULL));
	test_array_setup();

	mu_suite_start();
	
	mu_run_test(makeheap);
    mu_run_test(insertion);
    mu_run_test(delmin);
    mu_run_test(test_destroy);
	mu_run_test(test_heapsort);    
	mu_run_test(test_heapsort_inplace);

	return NULL;
}

RUN_TESTS(all_tests);

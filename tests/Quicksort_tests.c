#include <IOAlg/minunit.h>
#include <IOAlg/dbg.h>
#include <IOAlg/bool.h>
#include <IOAlg/Quicksort.h>
#include <time.h>
#include <stdlib.h>

#define TEST_SIZE 12000000
#define BLOCK_SIZE sizeof(int)

static int* test_array;

void test_array_setup()
{
  test_array = malloc(sizeof(int)*TEST_SIZE);
  int index = 0;
  for(index = 0; index < TEST_SIZE; index++)
	{
      test_array[index] = rand();
	}
}

char *quicksorting()
{
  quicksort(test_array, TEST_SIZE);

  int index;
  for (index=0; index < TEST_SIZE-1; index++){
    mu_assert(test_array[index] <= test_array[index+1], "Nope nope nope");
  }
  
  return NULL;
}

char *all_tests()
{
  srand(time(NULL));
  test_array_setup();

  mu_suite_start();
	
  mu_run_test(quicksorting);
  free(test_array);

  return NULL;
}

RUN_TESTS(all_tests);

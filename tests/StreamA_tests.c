#include <IOAlg/StreamA.h>
#include <IOAlg/minunit.h>
#include <IOAlg/dbg.h>
#include <IOAlg/bool.h>
#include <time.h>
#include <stdlib.h>

#define TEST_SIZE 100
#define BLOCK_SIZE sizeof(int)

static const char *test_file = "tests/testfiles/teststreamA";
static OutputStreamA *ost = NULL;
static InputStreamA *ist = NULL;
static int test_array[TEST_SIZE] = {0};

void test_array_setup()
{
	int index = 0;
	for(index = 0; index < TEST_SIZE; index++)
	{
		test_array[index] = rand();
	}
}

char *test_create()
{
	ost = OutputStreamA_create(test_file,BLOCK_SIZE);
	mu_assert(ost != NULL, "Failed to create Stream.");

	return NULL;
}

char *test_open()
{
	ist  = InputStreamA_open(test_file,BLOCK_SIZE);
	mu_assert(ist != NULL, "Failed to open stream for reading.");

	return NULL;
}

char *test_write_int()
{
	int rc = -1;
	int index = 0;

	for(index = 0; index < TEST_SIZE; index++)
	{
		rc = OutputStreamA_write(ost,&test_array[index]);
		mu_assert(rc == BLOCK_SIZE, "Failed to write to stream.");
	}

	return NULL;
}

char *test_read_int()
{
	int rc = -1;
	int index = 0;

	int result[TEST_SIZE+1] = {0};

	for(index = 0; index < TEST_SIZE; index++)
	{
		rc = InputStreamA_read_next(ist,&result[index]);
		mu_assert(rc == BLOCK_SIZE, "Failed to read from stream.");
		int expected = test_array[index];
		int res = result[index];
		mu_assert(expected == res, "Read should be equal to what was written.");
	}

	//The stream should be at EOF now, lets check the corresponding fnc!
	//Try to read, should fail and set the ->eof flag
	rc = InputStreamA_read_next(ist,&result[index+1]);
	rc = InputStreamA_end_of_stream(ist);
	int expected = TRUE; //Should be true that we are at end of file. 
	mu_assert(rc == expected, "Should be at EOF!");	

	return NULL;
}

char *test_close_output()
{
	int rc = OutputStreamA_close(ost);
	mu_assert(rc == 0, "Failed to close stream.");
	return NULL;
}

char *test_close_input()
{
	int rc = InputStreamA_close(ist);
	mu_assert(rc == 0, "Failed to close stream.");
	return NULL;

}

/* tests are interdependant and should run in the given order */
/* not pretty, consider using seperate stream structs pr test */
char *all_tests()
{
	srand(time(NULL));
	test_array_setup();

	mu_suite_start();
	
	mu_run_test(test_create);

	mu_run_test(test_write_int);	

	mu_run_test(test_open);
	mu_run_test(test_read_int);

	mu_run_test(test_close_output);
	mu_run_test(test_close_input);

	return NULL;
}

RUN_TESTS(all_tests);

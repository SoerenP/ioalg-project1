#include <IOAlg/StreamB.h>
#include <IOAlg/minunit.h>
#include <time.h>
#include <stdlib.h>

#define TEST_SIZE 100
#define BLOCK_SIZE 4


static const char *filename = "./tests/testfiles/testStreamB.txt";
static InputStreamB *ist = NULL;
static OutputStreamB *ost = NULL;
static int test_array[TEST_SIZE] = {0};

void setup_array()
{
	int index = 0;
	for(index = 0; index < TEST_SIZE; index++)
	{
		test_array[index] = rand();
	}
}

char *test_create()
{
	ost = OutputStreamB_create(filename, BLOCK_SIZE, TEST_SIZE);
	mu_assert(ost != NULL, "Failed to create OutputStreamB.");

	return NULL;
}


char *test_closei()
{
	int rc = InputStreamB_close(ist);
	mu_assert(rc != STREAM_ERR, "Failed to close InputStreamB.");

	return NULL;
}

char *test_closeo()
{
	int rc = OutputStreamB_close(ost);
	mu_assert(rc != STREAM_ERR, "Failed to close OutputStreamB.");

	return NULL;
}


char *test_open()
{
  ist = InputStreamB_open(filename, BLOCK_SIZE, TEST_SIZE);
	mu_assert(ist != NULL, "Failed to open InputStreamB.");

	return NULL;
}


char *test_write()
{
	int rc = OutputStreamB_write(ost, test_array, TEST_SIZE);
	mu_assert(rc == TEST_SIZE, "Failed to write desired amount.");

	return NULL;
}

char *test_read()
{
	int result_array[TEST_SIZE] = {0};
	int rc = InputStreamB_read_next(ist, result_array, TEST_SIZE);
	mu_assert(rc == TEST_SIZE, "Failed to read desired amount.");

	int index = 0;
	for(index = 0; index < TEST_SIZE; index++)
	{
		int expected = test_array[index];
		int result = result_array[index];
		mu_assert(result == expected, "Failed to read correct number.");
	}

	//Should be at the end of the stream after one more read, lets check!
	/* this will make the implementation throw an error, which should happen */
	rc = InputStreamB_read_next(ist, result_array,TEST_SIZE);
	mu_assert(rc == STREAM_ERR, "Should fail to read the file.");

	//now EOF Should be set after failed read attempt!
	int is_eof = InputStreamB_end_of_stream(ist);
	mu_assert(is_eof == 1, "Stream should be at EOF.");

	return NULL;
}

char *all_tests()
{
	/* setup */
	srand(time(NULL));
	setup_array();

	/* run tests */
	mu_suite_start();

	mu_run_test(test_create);
	mu_run_test(test_write);
	mu_run_test(test_closeo);

	mu_run_test(test_open);
	mu_run_test(test_read);
    	mu_run_test(test_closei);

	return NULL;
}

RUN_TESTS(all_tests);

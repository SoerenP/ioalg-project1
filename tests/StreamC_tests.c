#include <IOAlg/StreamC.h>
#include <IOAlg/minunit.h>
#include <IOAlg/bool.h>
#include <stdlib.h>
#include <time.h>


#define TEST_SIZE 11111
#define BUFFER_SIZE 500
#define BLOCK_SIZE sizeof(int)

static OutputStreamC *ost = NULL;
static InputStreamC *ist = NULL;
static const char *test_file = "./tests/testfiles/teststreamC";
static int *test_array = NULL;

void test_array_setup()
{
	test_array = malloc(sizeof(int) * TEST_SIZE);
	if(!(test_array)) exit(1);

	int index = 0;
	for(index = 0; index < TEST_SIZE; index++)
	{
		test_array[index] = rand();
	}
}

void test_array_cleanup()
{
	if(test_array) free(test_array);
}
char *test_create()
{
	ost = OutputStreamC_create(test_file, BLOCK_SIZE, BUFFER_SIZE);
	mu_assert(ost != NULL, "Failed to create StreamC.");

	return NULL;
}

char *test_write_int()
{
	int rc = -1;
	int index = 0;

	for(index = 0; index < TEST_SIZE; index++)
	{
		rc = OutputStreamC_write(ost, &test_array[index]);
		mu_assert(rc == BLOCK_SIZE, "Failed to write to StreamC.");
	}

	return NULL;
}

char *test_read_with_eof()
{
	int index = 0;
	int result[TEST_SIZE] = {0};
	int rc = -1;

	while(1){
		rc = InputStreamC_read_next(ist,&result[index]);
		mu_assert(rc == BLOCK_SIZE, "Failed to read to streamc.");
		index++;

		if(InputStreamC_end_of_stream(ist) != FALSE){
			break;
		}
	}
	mu_assert(index == TEST_SIZE, "Should have read all of the stream.");

	return NULL;
}

char *test_read_int()
{
	int rc = -1; 
	int index = 0;
	
	int result[TEST_SIZE] = {0};

	for(index = 0; index < TEST_SIZE; index++)
	{
		rc = InputStreamC_read_next(ist,&result[index]);
		mu_assert(rc == BLOCK_SIZE, "Failed to read from stream");

		int expected = test_array[index];
		int res = result[index];
		mu_assert(expected == res,
			"Read should be equal to what was written.");
	}

	int buffer;
	rc = InputStreamC_read_next(ist,&buffer);
	mu_assert(InputStreamC_end_of_stream(ist) == TRUE, "Should be at end of stream");

	return NULL;
}

char *test_open()
{
	ist = InputStreamC_open(test_file, BLOCK_SIZE, BUFFER_SIZE);
	mu_assert(ist != NULL, "Failed to open stream for reading.");

	return NULL;
}

char *test_close_output()
{
	int rc = OutputStreamC_close(ost);
	mu_assert(rc != -1, "Failed to close stream.");

	return NULL;
}

char *test_close_input()
{
	int rc = InputStreamC_close(ist);
	mu_assert(rc != -1, "Failed to close stream.");

	return NULL;
}

char *all_tests()
{
	srand(time(NULL));
	test_array_setup();

	mu_suite_start();	

	mu_run_test(test_create);
	
	mu_run_test(test_write_int);
	mu_run_test(test_close_output);

	mu_run_test(test_open);
	mu_run_test(test_read_int);

	mu_run_test(test_close_input);

	mu_run_test(test_open);
	mu_run_test(test_read_with_eof);
	mu_run_test(test_close_input);

	test_array_cleanup();

	return NULL;
}

RUN_TESTS(all_tests);

#include <IOAlg/StreamD.h>
#include <IOAlg/minunit.h>
#include <stdlib.h>
#include <time.h>
#include <IOAlg/bool.h>
#include <IOAlg/StreamB.h>

#define TEST_SIZE 91423
#define BUFFER_SIZE 4096
#define BLOCK_SIZE sizeof(int)

static InputStreamD *ist = NULL;
static OutputStreamD *ost = NULL;
static const char *test_file = "./tests/testfiles/streamdbogus.txt";
static const char *test_file_2 = "./tests/testfiles/streamdbogus2.txt";
static int* test_array;

void test_array_setup()
{
  test_array = malloc(sizeof(*test_array)*(TEST_SIZE));
  int index = 0;
  for(index = 0; index < TEST_SIZE; index++)
	{
      test_array[index] = rand() + 1; // avoid 0's
	}
}

char *test_create()
{
  ost = OutputStreamD_create(test_file, 0);
  mu_assert(ost != NULL, "Failed to create StreamD.");

  return NULL;
}

char *test_write_int()
{
  int rc = -1;
  rc = OutputStreamD_write(ost, test_array, TEST_SIZE);
  mu_assert(rc == STREAM_OK, "Failed to write to StreamD.");
  return NULL;
}


char *test_open()
{
  ist = InputStreamD_open(test_file, 0);
  mu_assert(ist != NULL, "Failed to open stream for reading.");

  return NULL;
}

char *test_read_int_otherstream(){
  int index = 0;
  
  int result[TEST_SIZE] = {0};
  InputStreamB *input = InputStreamB_open(test_file, sizeof(int), TEST_SIZE);
  InputStreamB_read_next(input, result, TEST_SIZE);

  for(index = 0; index < TEST_SIZE; index++)
	{
      int expected = test_array[index];
      int res = result[index];
      //fprintf(stderr, "expected: %d, got: %d \n", expected, res);
      if (expected != res){
        mu_assert(expected == res, "Read should be equal to what was written.");
      }
    }

  InputStreamB_close(input);
  return NULL;
}

char *test_read_int()
{
  int rc = -1;
  int index = 0;
	
  int result[TEST_SIZE] = {0};

  for(index = 0; index < TEST_SIZE; index++)
	{
      rc = InputStreamD_read_next(ist, &result[index]);
      mu_assert(rc == BLOCK_SIZE, "Failed to read from stream");

      int expected = test_array[index];
      int res = result[index];
      
      if (expected != res){
        fprintf(stderr, "Failed at index %d \n", index);
        fprintf(stderr, "expected: %d, got: %d \n", expected, res);
        fprintf(stderr, "filename: %s \n blocksize: %zu, \n elementsize: %zu \n fd: %d \n, filesize: %zd \n currentmap: %p\n offset: %d\n currentpos: %zu \n",
                ist->filename,
                ist->blocksize,
                ist->elementsize,
                ist->fd,
                ist->filesize,
                ist->currentmap,
                ist->offset,
                ist->currentpos);
        
        mu_assert(expected == res, "Read should be equal to what was written.");
      }
    }
  return NULL;
}


char *test_close()
{
  int rc = InputStreamD_close(ist);
  mu_assert(rc != -1, "Failed to close istream.");

  return NULL;
}

char *test_close2()
{
  int rc = OutputStreamD_close(ost);
  mu_assert(rc != -1, "Failed to close ostream.");

  return NULL;
}

char *test_read_write(){
  int rc = -1;

  // write random array to test file
  OutputStreamD *output = OutputStreamD_create(test_file_2, 0);
  mu_assert(output != NULL, "Failed to create StreamD.");

  rc = OutputStreamD_write(output, test_array, TEST_SIZE);
  mu_assert(rc == STREAM_OK, "Failed to write to StreamD.");

  rc = OutputStreamD_close(output);
  mu_assert(rc != -1, "Failed to close ostream.");

  // read it again
  InputStreamD *input = InputStreamD_open(test_file_2, 0);

  int index = 0;
  int result[TEST_SIZE << 1] = {0};
  while (InputStreamD_end_of_stream(input) == FALSE){
    rc = InputStreamD_read_next(input, &result[index]);
    index++;
  }

  InputStreamD_close(input);

  for (index = 0; index < TEST_SIZE; index++){
    int a = result[index];
    int b = test_array[index];
    //fprintf(stderr, "expected: %d, got: %d \n", expected, res);
    mu_assert(a == b, " wrong values in read vs write ");
  }

  return NULL;
}

char *all_tests()
{
	srand(time(NULL));
	test_array_setup();

	mu_suite_start();	

	mu_run_test(test_create);
	mu_run_test(test_write_int);
    mu_run_test(test_close2);

    mu_run_test(test_read_int_otherstream);
    
	mu_run_test(test_open);
	mu_run_test(test_read_int);
	mu_run_test(test_close);

    mu_run_test(test_read_write);

    free(test_array);
	return NULL;
}

RUN_TESTS(all_tests);

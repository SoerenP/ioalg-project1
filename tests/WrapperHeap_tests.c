#include <IOAlg/WrapperHeap.h>
#include <IOAlg/minunit.h>
#include <IOAlg/dbg.h>
#include <IOAlg/bool.h>
#include <time.h>
#include <stdlib.h>

#define TEST_SIZE 256
#define BLOCK_SIZE sizeof(int)

static WrapperHeap *Q;
static Wrapper test_array[TEST_SIZE];

void test_array_setup()
{
	int index = 0;
	for(index = 0; index < TEST_SIZE; index++)
	{
      test_array[index] = wrapper_heap_makeWrapper(rand(), 0);
	}
}

char *test_destroy(){
  wrapper_heap_destroy(Q);
  return NULL;
}

char *makeheap()
{
  Q = wrapper_heap_makeHeap();
  mu_assert(NULL != Q, "Failed to make heap, fuck..");

  return NULL;
}

char *insertion(){
  int index;
  for (index = 0; index < TEST_SIZE; index++){
    mu_assert(WRAPPER_HEAP_TRUE == wrapper_heap_insert(Q, test_array[index]), "could not insert into heap");
  }

  return NULL;
}

char *delmin(){
  int index;
  for (index = 0; index < TEST_SIZE; index++){
    test_array[index] = wrapper_heap_deleteMin(Q);
  }

  for (index = 0; index < TEST_SIZE-1; index++){
    mu_assert(test_array[index].key <= test_array[index+1].key, "array was not sorted");
  }
  return NULL;
}

char *test_heapsort_inplace()
{
	test_array_setup();

	wrapper_heapsort_inplace(test_array, TEST_SIZE);

	int index = 0;
	for(index = 0; index < TEST_SIZE-1; index++)
	{
		mu_assert(test_array[index].key <= test_array[index+1].key, "Array was not sorted");
	}

	return NULL;
}

char *test_heapsort()
{
	test_array_setup();

	Wrapper *sorted = wrapper_heapsort(test_array, TEST_SIZE);
	mu_assert(sorted != NULL, "Failed to return array from heapsort.");

	int index = 0;
	for(index = 0; index < TEST_SIZE-1; index++)
	{
		mu_assert(sorted[index].key <= sorted[index+1].key, "Array was not sorted");
	}	

	free(sorted);

	return NULL;
}



/* tests are interdependant and should run in the given order */
/* not pretty, consider using seperate stream structs pr test */
char *all_tests()
{
	srand(time(NULL));
	test_array_setup();

	mu_suite_start();
	
	mu_run_test(makeheap);
    mu_run_test(insertion);
    mu_run_test(delmin);
    mu_run_test(test_destroy);
	mu_run_test(test_heapsort);    
	mu_run_test(test_heapsort_inplace);

	return NULL;
}

RUN_TESTS(all_tests);

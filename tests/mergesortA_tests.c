#include <IOAlg/Mergesort.h>
#include <IOAlg/minunit.h>
#include <IOAlg/dbg.h>
#include <IOAlg/bool.h>
#include <IOAlg/Quicksort.h>
#include <time.h>
#include <stdlib.h>

#define N 1024 // number of total elements
#define B 128  // block size
#define M 64   // size of each filestream
#define d 2    // d streams to be merged 

static const char *test_file_in = "tests/testfiles/merge_in.txt";
static const char *test_file_out = "tests/testfiles/merge_out.txt";

static int* test_array;
static int* sorted_test_array;

void test_array_setup()
{
  test_array = malloc(sizeof(int)*N);
  sorted_test_array = malloc(sizeof(int)*N);
  int index = 0;
  for(index = 0; index < N; index++)
	{
      test_array[index] = rand();
      sorted_test_array[index] = test_array[index];
	}
  quicksort(sorted_test_array, N);
}

void test_file_setup(){
  test_array_setup();
  OutputStreamA *output = OutputStreamA_create(test_file_in, sizeof(int));
  StreamA_write_array(output, test_array, N);
  OutputStreamA_close(output);
}

char *test_sorted_files()
{
  int err = mergesort_ext_A(test_file_in, test_file_out, N, M, d);
  mu_assert(err == MERGE_OK, "Failed to do stuff.");

  // are they equal? **drumroll**
  InputStreamA *input = InputStreamA_open(test_file_out, sizeof(int));
  mu_assert(input != NULL, "input stream was null");
  
  int result[N] = {0};
  int index;
  for(index = 0; index < N; index++)
	{
      InputStreamA_read_next(input, &result[index]);
      int expected = sorted_test_array[index];
      int res = result[index];
      mu_assert(expected == res,
                "Quicksort and Mergesort should be equal");
	}

  InputStreamA_close(input);
  return NULL;
}

char *test_sorted_files2()
{
  int err = mergesort_ext_A(test_file_in, test_file_out, N, M, 3);
  mu_assert(err == MERGE_OK, "Failed to do stuff.");

  // are they equal? **drumroll**
  InputStreamA *input = InputStreamA_open(test_file_out, sizeof(int));
  mu_assert(input != NULL, "input stream was null");
  
  int result[N] = {0};
  int index;
  for(index = 0; index < N; index++)
	{
      InputStreamA_read_next(input, &result[index]);
      int expected = sorted_test_array[index];
      int res = result[index];
      //fprintf(stderr, "iteration: %d, expected: %d, got: %d \n", index, expected, res);
      mu_assert(expected == res,
                "Read should be equal to what was written.");
	}

  InputStreamA_close(input);
  return NULL;
}



char *test_sorted_files3()
{
  int err = mergesort_ext_A(test_file_in, test_file_out, N, M, 7);
  mu_assert(err == MERGE_OK, "Failed to do stuff.");

  // are they equal? **drumroll**
  InputStreamA *input = InputStreamA_open(test_file_out, sizeof(int));
  mu_assert(input != NULL, "input stream was null");
  
  int result[N] = {0};
  int index;
  for(index = 0; index < N; index++)
	{
      InputStreamA_read_next(input, &result[index]);
      int expected = sorted_test_array[index];
      int res = result[index];
      //fprintf(stderr, "iteration: %d, expected: %d, got: %d \n", index, expected, res);
      mu_assert(expected == res,
                "Read should be equal to what was written.");
	}

  InputStreamA_close(input);
  return NULL;
}


char *all_tests()
{
  srand(time(NULL));
  test_file_setup();
  
  mu_suite_start();
    
  mu_run_test(test_sorted_files);
  mu_run_test(test_sorted_files2);
  mu_run_test(test_sorted_files3);

  return NULL;
}

RUN_TESTS(all_tests);

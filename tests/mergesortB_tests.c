#include <IOAlg/Mergesort.h>
#include <IOAlg/StreamB.h>
#include <IOAlg/minunit.h>
#include <IOAlg/dbg.h>
#include <IOAlg/bool.h>
#include <IOAlg/Quicksort.h>
#include <time.h>
#include <stdlib.h>

#define N 1024
#define M 64
#define d 2
/* virker kun ot hvis M*D = N, dvs else casen i sammenfletningsloopet virker ikke */

static const char *test_file_in = "tests/testfiles/streambmergein";
static const char *test_file_out = "tests/testfiles/streambmergeout";

static int *test_array;
static int *sorted_test_array;

// unused
/* static void print_array_in_test(int *A, int size) */
/* { */
/* 	int index; */
/* 	for(index = 0; index < size; index++) */
/* 	{ */
/* 		printf("%d ",A[index]); */
/* 	} */
/* 	printf("\n"); */
/* } */

void test_array_destroy()
{
	if(test_array) free(test_array);
	if(sorted_test_array) free(sorted_test_array);
}

void test_array_setup()
{
	test_array = malloc(sizeof(int)*N);
	sorted_test_array = malloc(sizeof(int)*N);
	int index = 0;
	for(index = 0; index < N; index++)
	{
		test_array[index] = rand();
		sorted_test_array[index] = test_array[index];
	}
	quicksort(sorted_test_array,N);
}

void test_file_setup()
{
	test_array_setup();
	OutputStreamB *output = OutputStreamB_create(test_file_in, sizeof(int),N);

	OutputStreamB_write(output,test_array,N);
	OutputStreamB_close(output);
}

char *test_merge_sort_ext()
{
	int err = mergesort_ext_B(test_file_in, test_file_out, N, M, d);
	mu_assert(err == MERGE_OK, "Failed to mergesort external.");
	InputStreamB *input = InputStreamB_open(test_file_out, sizeof(int), N);

	mu_assert(input != NULL, "Could not open input stream.");
	int *result = malloc(sizeof(int) * N);

	int index = 0;
	for(index = 0; index < N; index++)
	{
		int expected = sorted_test_array[index];
		int res;
		InputStreamB_read_next(input,&res,1); //todo could just read array at start
        fprintf(stderr,"expected %d but got %d , and index %d \n ", expected, res, index);
		mu_assert(expected == res, "External Mergesort & quicksort should give the same.");
	}

	InputStreamB_close(input);

	free(result);
	return NULL;

}


char *test_merge_sort_ext_3()
{
	int temp_d = 3;

	int err = mergesort_ext_B(test_file_in, test_file_out, N, M, temp_d);
	mu_assert(err == MERGE_OK, "Failed to mergesort external.");
	InputStreamB *input = InputStreamB_open(test_file_out, sizeof(int), N);

	mu_assert(input != NULL, "Could not open input stream.");
	int *result = malloc(sizeof(int) * N);

	InputStreamB_read_next(input,result,N);
	int index = 0;
	for(index = 0; index < N; index++)
	{
		int expected = sorted_test_array[index];
		int res = result[index];
		mu_assert(expected == res, "External Mergesort & quicksort should give the same.");
	}

	InputStreamB_close(input);

	free(result);
	return NULL;
}


char *test_merge_sort_ext_7()
{
	int temp_d = 7;

	int err = mergesort_ext_B(test_file_in, test_file_out, N, M, temp_d);
	mu_assert(err == MERGE_OK, "Failed to mergesort external.");
	InputStreamB *input = InputStreamB_open(test_file_out, sizeof(int), N);

	mu_assert(input != NULL, "Could not open input stream.");
	int *result = malloc(sizeof(int) * N);

	InputStreamB_read_next(input,result,N);
	int index = 0;
	for(index = 0; index < N; index++)
	{
		int expected = sorted_test_array[index];
		int res = result[index];
		mu_assert(expected == res, "External Mergesort & quicksort should give the same.");
	}

	InputStreamB_close(input);

	free(result);
	return NULL;
}


char *all_tests()
{
	srand(time(NULL));
	test_file_setup();

	mu_suite_start();
	mu_run_test(test_merge_sort_ext);
	mu_run_test(test_merge_sort_ext_3);
	mu_run_test(test_merge_sort_ext_7);

	test_array_destroy();

	return NULL;
}

RUN_TESTS(all_tests);

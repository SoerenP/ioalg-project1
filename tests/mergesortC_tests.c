#include <IOAlg/Mergesort.h>
#include <IOAlg/minunit.h>
#include <IOAlg/dbg.h>
#include <IOAlg/bool.h>
#include <IOAlg/Quicksort.h>
#include <time.h>
#include <stdlib.h>

#define N 1024 //total nr elements
#define B 128 // block size
#define M 64 // size of each filestream
#define d 2 //d streams to be merged

static const char *test_file_in = "tests/testfiles/streamcmergein";
static const char *test_file_out = "tests/testfiles/streamcmergeout";

static int *test_array;
static int *sorted_test_array;

void print_array(int *A, int size)
{
	int index;
	for(index = 0; index < size; index++)
	{
		printf("%d ",A[index]);
	}
	printf("\n");
}

void test_array_destroy()
{
	if(test_array) free(test_array);
	if(sorted_test_array) free(sorted_test_array);
}

void test_array_setup()
{
	test_array = malloc(sizeof(int)*N);
	sorted_test_array = malloc(sizeof(int)*N);
	int index = 0;
	for(index = 0; index < N; index++)
	{
		test_array[index] = rand();
		sorted_test_array[index] = test_array[index];
	}
	quicksort(sorted_test_array,N);
}

void test_file_setup()
{
	test_array_setup();
	OutputStreamC *output = OutputStreamC_create(test_file_in,sizeof(int),B);
	StreamC_write_array(output,test_array,N);
	OutputStreamC_close(output);
}

char *test_merge_sort_ext()
{
	int err = mergesort_ext_C(test_file_in, test_file_out, N, M, d, B);
	mu_assert(err == MERGE_OK, "Failed to mergesort external");

	InputStreamC *input = InputStreamC_open(test_file_out, sizeof(int), B);
	mu_assert(input != NULL, "Input stream was null.");

	int result[N] = {0};
	int index;
	for(index = 0; index < N; index++)
	{
		InputStreamC_read_next(input, &result[index]);
		int expected = sorted_test_array[index];
		int res = result[index];
		mu_assert(expected == res, "External Mergesort and quicksort should give the same.");
	}
	InputStreamC_close(input);

	return NULL;
}

char *test_merge_sort_ext_dis3()
{
	int temp_d = 3;

	int err = mergesort_ext_C(test_file_in, test_file_out, N, M, temp_d, B);
	mu_assert(err == MERGE_OK, "Failed to mergesort external");

	InputStreamC *input = InputStreamC_open(test_file_out, sizeof(int), B);
	mu_assert(input != NULL, "Input stream was null.");

	int result[N] = {0};
	int index;
	for(index = 0; index < N; index++)
	{
		InputStreamC_read_next(input, &result[index]);
		int expected = sorted_test_array[index];
		int res = result[index];
		mu_assert(expected == res, "External Mergesort and quicksort should give the same.");
	}
	InputStreamC_close(input);
	return NULL;
}


char *test_merge_sort_ext_dis7()
{
	int temp_d = 7;

	int err = mergesort_ext_C(test_file_in, test_file_out, N, M, temp_d, B);
	mu_assert(err == MERGE_OK, "Failed to mergesort external");

	InputStreamC *input = InputStreamC_open(test_file_out, sizeof(int), B);
	mu_assert(input != NULL, "Input stream was null.");

	int result[N] = {0};
	int index;
	for(index = 0; index < N; index++)
	{
		InputStreamC_read_next(input, &result[index]);
		int expected = sorted_test_array[index];
		int res = result[index];
		mu_assert(expected == res, "External Mergesort and quicksort should give the same.");
	}
	InputStreamC_close(input);
	return NULL;
}

char *all_tests()
{
	srand(time(NULL));
	test_file_setup();

	mu_suite_start();
	mu_run_test(test_merge_sort_ext);
	mu_run_test(test_merge_sort_ext_dis3);
	mu_run_test(test_merge_sort_ext_dis7);

	test_array_destroy();

	return NULL;
}

RUN_TESTS(all_tests);

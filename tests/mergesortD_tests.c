#include <IOAlg/Mergesort.h>
#include <IOAlg/minunit.h>
#include <IOAlg/dbg.h>
#include <IOAlg/bool.h>
#include <IOAlg/Quicksort.h>
#include <time.h>
#include <stdlib.h>

#define N 1024 // number of total elements
#define B 4096  // block size
#define M 256  // size of each filestream
#define d 2     // d streams to be merged 

static const char *test_file_in = "tests/testfiles/merge_inD.txt";
static const char *test_file_out = "tests/testfiles/merge_outD.txt";

static int* test_array;
static int* sorted_test_array;

void test_array_setup()
{
  test_array = malloc(sizeof(int)*N);
  sorted_test_array = malloc(sizeof(int)*N);
  int index = 0;
  for(index = 0; index < N; index++)
	{
      test_array[index] = rand() + 1;
      sorted_test_array[index] = test_array[index];
	}
  quicksort(sorted_test_array, N);
}

void test_file_setup(){
  test_array_setup();
  OutputStreamD *output = OutputStreamD_create(test_file_in, B);
  OutputStreamD_write(output, test_array, N);
  OutputStreamD_close(output);
}

char *test_sorted_files()
{
  int err = mergesort_ext_D(test_file_in, test_file_out, N, M, d, B);
  mu_assert(err == MERGE_OK, "Failed to do stuff.");

  // are they equal? **drumroll**
  InputStreamD *input = InputStreamD_open(test_file_out, B);
  mu_assert(input != NULL, "input stream was null");

  int result[N] = {0};      
  int index = 0;
  for(index = 0; index < N; index++){
      InputStreamD_read_next(input, &result[index]);
      
      int expected = sorted_test_array[index];
      int res = result[index];
      if (expected != res){
        fprintf(stderr, "expected: %d, but got %d \n index was : %d \n ", expected, res, index);
        mu_assert(expected == res,
                "Quicksort and Mergesort should be equal");
      }
	}

  InputStreamD_close(input);
  return NULL;
}

char *test_sorted_files_d5()
{
  int err = mergesort_ext_D(test_file_in, test_file_out, N, M, 5, B);
  mu_assert(err == MERGE_OK, "Failed to do stuff.");

  // are they equal? **drumroll**
  InputStreamD *input = InputStreamD_open(test_file_out, B);
  mu_assert(input != NULL, "input stream was null");

  int result[N] = {0};      
  int index = 0;
  for(index = 0; index < N; index++){
      InputStreamD_read_next(input, &result[index]);
      
      int expected = sorted_test_array[index];
      int res = result[index];
      if (expected != res){
        fprintf(stderr, "expected: %d, but got %d \n index was : %d \n ", expected, res, index);
        mu_assert(expected == res,
                "Quicksort and Mergesort should be equal");
      }
	}

  InputStreamD_close(input);
  return NULL;
}

char *test_sorted_files_d7()
{
  int err = mergesort_ext_D(test_file_in, test_file_out, N, M, 7, B);
  mu_assert(err == MERGE_OK, "Failed to do stuff.");

  // are they equal? **drumroll**
  InputStreamD *input = InputStreamD_open(test_file_out, B);
  mu_assert(input != NULL, "input stream was null");

  int result[N] = {0};      
  int index = 0;
  for(index = 0; index < N; index++){
      InputStreamD_read_next(input, &result[index]);
      
      int expected = sorted_test_array[index];
      int res = result[index];
      if (expected != res){
        fprintf(stderr, "expected: %d, but got %d \n index was : %d \n ", expected, res, index);
        mu_assert(expected == res,
                "Quicksort and Mergesort should be equal");
      }
	}

  InputStreamD_close(input);
  return NULL;
}

char *test_sorted_files_N726()
{
  int err = mergesort_ext_D(test_file_in, test_file_out, 726, M, d, B);
  mu_assert(err == MERGE_OK, "Failed to do stuff.");

  // are they equal? **drumroll**
  InputStreamD *input = InputStreamD_open(test_file_out, B);
  mu_assert(input != NULL, "input stream was null");

  int result[726] = {0};      
  int index = 0;
  for(index = 0; index < 726; index++){
      InputStreamD_read_next(input, &result[index]);
      
      int expected = sorted_test_array[index];
      int res = result[index];
      if (expected != res){
        fprintf(stderr, "expected: %d, but got %d \n index was : %d \n ", expected, res, index);
        mu_assert(expected == res,
                "Quicksort and Mergesort should be equal");
      }
	}

  InputStreamD_close(input);
  return NULL;
}


char *sanity(){ return NULL; }

char *all_tests()
{
  srand(time(NULL));
  test_file_setup();
  
  mu_suite_start();

  // The following tests have been commented,
  // As the implementation of Mergesort for Stream D is incomplete.
  mu_run_test(sanity);
  /* mu_run_test(test_sorted_files); */
  /* mu_run_test(test_sorted_files_d5); */
  /* mu_run_test(test_sorted_files_d7); */
  //  mu_run_test(test_sorted_files_N726);
  /* mu_run_test(test_sorted_files2); */
  /* mu_run_test(test_sorted_files3); */

  return NULL;
}

RUN_TESTS(all_tests);

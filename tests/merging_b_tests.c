#include <IOAlg/StreamB.h>
#include <IOAlg/minunit.h>
#include <IOAlg/Quicksort.h>
#include <IOAlg/merging.h>
#include <stdlib.h>
#include <time.h>

#define D 3
#define ARR_SIZE 1000
#define ELEMENT_SIZE (sizeof(int))
#define PATH_SIZE 100


static int test_counter = 0;
static char *test_path = "tests/testfiles/teststream";

void test_counter_reset()
{
	test_counter = 0;
}

void next_stream_name(char *buffer, size_t strlen, const char *prefix)
{
	memset(buffer,0,strlen);

	snprintf(buffer,sizeof(char)*PATH_SIZE,"%s%d",prefix,test_counter);

	test_counter++;
}

void get_random_array(int *A, int size)
{
	int index = 0;
	for(index = 0; index < size; index++)
	{
		A[index] = rand();
	}
}

char *test_merging_different_size()
{

	char buffer[PATH_SIZE] = {0};

	const char *result_stream = "tests/testfiles/result_dif";
	const char *stream_paths = "tests/testfiles/teststream_dif";

	OutputStreamB *o_sorted_streams[D];

	int random_sizes[D] = {0};

	int index = 0;
	for(index = 0; index < D; index++)
	{
		/* get random array and sort it */
		random_sizes[index] = rand() % ARR_SIZE;

		int *temp_array = malloc(sizeof(int) * random_sizes[index]);
		get_random_array(temp_array, random_sizes[index]);
		quicksort(temp_array, random_sizes[index]);
		
		/* create an output stream and insert the sorted array */

		next_stream_name(buffer,PATH_SIZE,stream_paths);
		
		o_sorted_streams[index] = 
		OutputStreamB_create(buffer,ELEMENT_SIZE,random_sizes[index]);
	
		OutputStreamB_write(o_sorted_streams[index],temp_array,random_sizes[index]);
		OutputStreamB_close(o_sorted_streams[index]);	

		free(temp_array);
	}

	/* now open the streams as input and give it to the merger */
	test_counter_reset();

	InputStreamB *i_sorted_streams[D];
	for(index = 0; index < D; index++)
	{
		next_stream_name(buffer,PATH_SIZE,stream_paths);

		i_sorted_streams[index] = InputStreamB_open(buffer,ELEMENT_SIZE,random_sizes[index]);
	}


	OutputStreamB *res = StreamB_merge(result_stream,i_sorted_streams,D,ELEMENT_SIZE);
	mu_assert(res != NULL, "Failed to merge streams");

	int res_size = res->num_elements;

	OutputStreamB_close(res);

	StreamB_close_input_streams(i_sorted_streams,D);

	/* read res array and test sorted order */

	int *result_array = malloc(ELEMENT_SIZE * res_size);

	InputStreamB *i_res = InputStreamB_open(result_stream,ELEMENT_SIZE,res_size);
	InputStreamB_read_next(i_res,result_array,i_res->num_elements);

	for(index = 0; index < res_size-1; index++)
	{
		mu_assert(result_array[index] <= result_array[index+1], "Failed to create sorted streams.");
	}

	free(result_array);
	InputStreamB_close(i_res);

	return NULL;
}


char *test_merging_same_size()
{
	test_counter_reset();

	char buffer[PATH_SIZE] = {0};

	const char *result_stream = "tests/testfiles/result";

	OutputStreamB *o_sorted_streams[D];

	int index = 0;
	for(index = 0; index < D; index++)
	{
		/* get random array and sort it */
		int *temp_array = malloc(sizeof(int) * ARR_SIZE);
		get_random_array(temp_array, ARR_SIZE);
		quicksort(temp_array, ARR_SIZE);
		
		/* create an output stream and insert the sorted array */
		next_stream_name(buffer,PATH_SIZE,test_path);
		o_sorted_streams[index] = 
		OutputStreamB_create(buffer,ELEMENT_SIZE,ARR_SIZE);
	
		OutputStreamB_write(o_sorted_streams[index],temp_array,o_sorted_streams[index]->num_elements);
		OutputStreamB_close(o_sorted_streams[index]);	
		free(temp_array);
	}

	/* now open the streams as input and give it to the merger */
	test_counter_reset();

	InputStreamB *i_sorted_streams[D];
	for(index = 0; index < D; index++)
	{
		next_stream_name(buffer,PATH_SIZE,test_path);
		i_sorted_streams[index] = InputStreamB_open(buffer,ELEMENT_SIZE,ARR_SIZE);
	
	}

	OutputStreamB *res = StreamB_merge(result_stream,i_sorted_streams,D,ELEMENT_SIZE);
	mu_assert(res != NULL, "Failed to merge streams");

	int res_size = res->num_elements;

	OutputStreamB_close(res);

	StreamB_close_input_streams(i_sorted_streams,D);

	/* read res array and test sorted order */

	int *result_array = malloc(ELEMENT_SIZE * res_size);
	InputStreamB *i_res = InputStreamB_open(result_stream,ELEMENT_SIZE,res_size);
	InputStreamB_read_next(i_res,result_array,i_res->num_elements);

	for(index = 0; index < res_size-1; index++)
	{
		mu_assert(result_array[index] <= result_array[index+1], "Failed to create sorted streams.");
	}

	free(result_array);
	InputStreamB_close(i_res);

	return NULL;
}

char *all_tests()
{
	srand(time(NULL));
	mu_suite_start();

	mu_run_test(test_merging_different_size);
	mu_run_test(test_merging_same_size);

	return NULL;
}


RUN_TESTS(all_tests);

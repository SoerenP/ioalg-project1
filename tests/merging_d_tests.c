#include <IOAlg/StreamD.h>
#include <IOAlg/minunit.h>
#include <IOAlg/Quicksort.h>
#include <IOAlg/merging.h>
#include <stdlib.h>
#include <time.h>

#define ARR_SIZE 1204
#define ELEMENT_SIZE (sizeof(int))
#define BUFFER_SIZE 4096
#define PATH_SIZE 100

static int test_counter = 0;
static char *test_path = "tests/testfiles/teststreamd";

void test_counter_reset()
{
  test_counter = 0;
}

void next_stream_name(char *buffer, size_t strlen, const char *prefix)
{
  memset(buffer,0,strlen);
  snprintf(buffer,sizeof(char)*PATH_SIZE,"%s%d",prefix,test_counter);
  test_counter++;
}

void get_random_array(int *A, int size)
{
  int index = 0;
  for(index = 0; index < size; index++)
    {
      A[index] = rand() + 1;
    }
}

char *test_merging_same_size_d12()
{
  int D = 12;
  char buffer[PATH_SIZE] = {0};

  const char *result_file = "tests/testfiles/resultd_dif";

  int index = 0;
  int *temp_array = malloc(sizeof(int) * ARR_SIZE);
  OutputStreamD *o_sorted_stream;
  // Create D files with sorted integers
  for(index = 0; index < D; index++)
    {
      get_random_array(temp_array, ARR_SIZE);
      quicksort(temp_array, ARR_SIZE);

      next_stream_name(buffer,PATH_SIZE,test_path);
      o_sorted_stream = OutputStreamD_create(buffer, BUFFER_SIZE);

      OutputStreamD_write(o_sorted_stream,temp_array, ARR_SIZE);

      OutputStreamD_close(o_sorted_stream);
    }
  free(temp_array);

  test_counter_reset();

  // Create D inputstreams with sorted integers to be read
  InputStreamD *i_sorted_streams[D];
  for(index = 0; index < D; index++)
    {
      next_stream_name(buffer,PATH_SIZE,test_path);
      i_sorted_streams[index] = InputStreamD_open(buffer, BUFFER_SIZE);
    }

  // Merge them.
  OutputStreamD *res = StreamD_merge(result_file, i_sorted_streams, D, ELEMENT_SIZE, BUFFER_SIZE);
  mu_assert(res != NULL, "Failed to Merge Streams.");
  OutputStreamD_close(res);

  int res_size = ARR_SIZE * D;
  int *result_array = malloc(ELEMENT_SIZE * res_size);
  InputStreamD *i_res = InputStreamD_open(result_file, BUFFER_SIZE);

  StreamD_read_array(i_res, result_array);

  for(index = 0; index < res_size-1; index++)
    {
      mu_assert(result_array[index] <= result_array[index+1], "Failed to create sorted stream.");
    }

  free(result_array);
  InputStreamD_close(i_res);

  return NULL;
}

char *test_merging_same_size_d7()
{
  int D = 7;
  char buffer[PATH_SIZE] = {0};

  const char *result_file = "tests/testfiles/resultd_dif7";

  int index = 0;
  int *temp_array = malloc(sizeof(int) * ARR_SIZE);
  OutputStreamD *o_sorted_stream;
  // Create D files with sorted integers
  for(index = 0; index < D; index++)
    {
      get_random_array(temp_array, ARR_SIZE);
      quicksort(temp_array, ARR_SIZE);

      next_stream_name(buffer,PATH_SIZE,test_path);
      o_sorted_stream = OutputStreamD_create(buffer, BUFFER_SIZE);

      OutputStreamD_write(o_sorted_stream,temp_array, ARR_SIZE);

      OutputStreamD_close(o_sorted_stream);
    }
  free(temp_array);

  test_counter_reset();

  // Create D inputstreams with sorted integers to be read
  InputStreamD *i_sorted_streams[D];
  for(index = 0; index < D; index++)
    {
      next_stream_name(buffer,PATH_SIZE,test_path);
      i_sorted_streams[index] = InputStreamD_open(buffer, BUFFER_SIZE);
    }

  // Merge them.
  OutputStreamD *res = StreamD_merge(result_file, i_sorted_streams, D, ELEMENT_SIZE, BUFFER_SIZE);
  mu_assert(res != NULL, "Failed to Merge Streams.");
  OutputStreamD_close(res);

  int res_size = ARR_SIZE * D;
  int *result_array = malloc(ELEMENT_SIZE * res_size);
  InputStreamD *i_res = InputStreamD_open(result_file, BUFFER_SIZE);

  StreamD_read_array(i_res, result_array);

  for(index = 0; index < res_size-1; index++)
    {
      mu_assert(result_array[index] <= result_array[index+1], "Failed to create sorted stream.");
    }

  free(result_array);
  InputStreamD_close(i_res);

  return NULL;
}

char *test_merging_same_size_d2()
{
  int D = 2;
  char buffer[PATH_SIZE] = {0};

  const char *result_file = "tests/testfiles/resultd_dif7";

  int index = 0;
  int *temp_array = malloc(sizeof(int) * ARR_SIZE);
  OutputStreamD *o_sorted_stream;
  // Create D files with sorted integers
  for(index = 0; index < D; index++)
    {
      get_random_array(temp_array, ARR_SIZE);
      quicksort(temp_array, ARR_SIZE);

      next_stream_name(buffer,PATH_SIZE,test_path);
      o_sorted_stream = OutputStreamD_create(buffer, BUFFER_SIZE);

      OutputStreamD_write(o_sorted_stream,temp_array, ARR_SIZE);

      OutputStreamD_close(o_sorted_stream);
    }
  free(temp_array);

  test_counter_reset();

  // Create D inputstreams with sorted integers to be read
  InputStreamD *i_sorted_streams[D];
  for(index = 0; index < D; index++)
    {
      next_stream_name(buffer,PATH_SIZE,test_path);
      i_sorted_streams[index] = InputStreamD_open(buffer, BUFFER_SIZE);
    }

  // Merge them.
  OutputStreamD *res = StreamD_merge(result_file, i_sorted_streams, D, ELEMENT_SIZE, BUFFER_SIZE);
  mu_assert(res != NULL, "Failed to Merge Streams.");
  OutputStreamD_close(res);

  int res_size = ARR_SIZE * D;
  int *result_array = malloc(ELEMENT_SIZE * res_size);
  InputStreamD *i_res = InputStreamD_open(result_file, BUFFER_SIZE);

  StreamD_read_array(i_res, result_array);

  for(index = 0; index < res_size-1; index++)
    {
      mu_assert(result_array[index] <= result_array[index+1], "Failed to create sorted stream.");
    }

  free(result_array);
  InputStreamD_close(i_res);

  return NULL;
}


char *test_merging_weird_sizes()
{
  int D = 2;
  int arr_size = 1024;
  char buffer[PATH_SIZE] = {0};

  const char *result_file = "tests/testfiles/resultd_difw";

  int index = 0;
  int *temp_array = malloc(sizeof(int) * arr_size);
  int totalsize = 0;
  // Create D files with sorted integers
  for(index = 0; index < D; index++)
    {
      OutputStreamD *o_sorted_stream;
      int randomsize = rand() % arr_size;
      totalsize += randomsize;
      get_random_array(temp_array, randomsize);
      quicksort(temp_array, randomsize);

      next_stream_name(buffer, PATH_SIZE, test_path);
      o_sorted_stream = OutputStreamD_create(buffer, BUFFER_SIZE);

      OutputStreamD_write(o_sorted_stream, temp_array, randomsize);

      OutputStreamD_close(o_sorted_stream);
    }
  free(temp_array);

  test_counter_reset();

  // Create D inputstreams with sorted integers to be read
  InputStreamD *i_sorted_streams[D];
  for(index = 0; index < D; index++)
    {
      next_stream_name(buffer, PATH_SIZE, test_path);
      i_sorted_streams[index] = InputStreamD_open(buffer, BUFFER_SIZE);
    }

  // Merge them.
  OutputStreamD *res = StreamD_merge(result_file, i_sorted_streams, D, ELEMENT_SIZE, BUFFER_SIZE);
  mu_assert(res != NULL, "Failed to Merge Streams.");
  OutputStreamD_close(res);

  fprintf(stderr, "totalsize is %d", totalsize);
  int *result_array = malloc(ELEMENT_SIZE * totalsize);
  InputStreamD *i_res = InputStreamD_open(result_file, BUFFER_SIZE);

  StreamD_read_array(i_res, result_array);

  for(index = 0; index < totalsize-1; index++) {
    mu_assert(result_array[index] <= result_array[index+1], "Failed to create sorted stream.");
  }
  
  InputStreamD_close(i_res);
  free(result_array);
  return NULL;
}

char *sanity() { return NULL; }

char *all_tests()
{
  srand(time(NULL));
  
  mu_suite_start();

  // The following tests have been commented,
  // As the implementation of merging for Stream D is incomplete.
  mu_run_test(sanity);
  /* mu_run_test(test_merging_same_size_d2); */
  /* mu_run_test(test_merging_same_size_d12); */
  /* mu_run_test(test_merging_same_size_d7); */
  /* mu_run_test(test_merging_weird_sizes); */
  
  return NULL;
}

RUN_TESTS(all_tests);
